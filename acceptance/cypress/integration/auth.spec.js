describe("Auth", () => {
  beforeEach(() => {
    cy.server();
    cy.route("POST", "/api/**").as("api");
  });

  let email = "editest@wipro.com";
  let password = "test";

  it("API endpoint should return error object with bad login details", () => {
    cy.request("POST", "/api/login", {
      wiproemail: "notaregistereduser@wipro.com",
      password: "test"
    }).then(response => {
      expect(response.body).to.have.property("status", "error");
    });
  });

  it("API endpoint should return user object on login", () => {
    cy.request("POST", "/api/login", {
      wiproemail: "editest@wipro.com",
      password: "test"
    }).then(response => {
      expect(response.body).to.have.property("name", "Mr test");
    });
  });

  it("Filling in the auth form with seed data should log you in", () => {
    cy.login({ email, password });
    cy.wait("@api");
    //header should show profile button
    cy.get('[data-testid="logout-btn"]').should("exist");
  });

  it("logged in refresh -> should stay logged in", () => {
    cy.visit("/");
    cy.login({ email, password });
    cy.wait("@api");
    cy.reload();
    // cy.wait("@api");
    cy.get('[data-testid="logout-btn"]').should("exist");
  });

  it("Logout should log you out", () => {
    cy.visit("/");
    cy.login({ email, password });
    cy.wait("@api");
    cy.get('[data-testid="logout-btn"]').click();
    cy.get('[data-testid="login-submit"]').should("exist");
  });
});
