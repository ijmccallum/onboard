describe("User Detail", () => {
  beforeEach(() => {
    cy.server();
    cy.route("POST", "/api/**").as("api");
  });

  let email = "editest@wipro.com";
  let password = "test";

  //open user from list page
  //open uesr directly

  it("Opening the user page directly and indirectly", () => {
    let crudUserData = {
      name: "cypress user",
      wiproemail: "cypress.user@wipro.com",
      password: "test",
      _id: ""
    };

    cy.visit("/");
    cy.login({ email, password });
    cy.wait("@api");
    cy.visit("/#/users");

    //fill in the new user form
    cy.createUser(crudUserData);
    cy.wait("@api").then(xhr => {
      crudUserData._id = xhr.response.body.user._id;
      //now to visit the user's detail page directly
      cy.visit("/");
      cy.reload();
      const directUrl = "/#/users/" + crudUserData._id;
      cy.visit(directUrl);

      cy.get('[data-testid="userobject-name"]').should("exist");

      //now to visit the user's page from the list
      cy.visit("/");
      cy.reload();
      cy.visit("/#/users");
      cy.get('[data-testid="user-list-item-name"]')
        .contains(crudUserData.name)
        .click();

      //and delete the user
      cy.get('[data-testid="userobject-delete-btn"]')
        .should("exist")
        .click();
    });
  });
});
