describe("User list", () => {
  beforeEach(() => {
    cy.server();
    cy.route("POST", "/api/**").as("api");
  });

  let email = "editest@wipro.com";
  let password = "test";

  //open user from list page
  //open uesr directly

  it("Create and remove a user", () => {
    const crudUserData = {
      name: "cypress user",
      wiproemail: "cypress.user@wipro.com",
      password: "test"
    };
    cy.visit("/");
    cy.login({ email, password });
    cy.wait("@api");
    cy.visit("/#/users");

    //fill in the new user form
    cy.createUser(crudUserData);
    cy.wait("@api");

    //the new user should have been added to the list of users
    cy.get('[data-testid="user-list-item-name"]')
      .contains(crudUserData.name)
      .should("exist");

    //try to add the same user again!
    cy.get('[data-testid="new-user-submit"]').click();
    cy.wait("@api");

    //check there's an error
    cy.get('[data-testid="new-user-serverror"]').should("exist");

    //go into the user's detail
    cy.get('[data-testid="user-list-item-name"]')
      .contains(crudUserData.name)
      .click();

    //and delete the user
    cy.get('[data-testid="userobject-delete-btn"]').click();

    //should be redirected back to the user list without the deleted user
    cy.get(`[data-testid=\"user-list-item-name\"]`)
      .contains(crudUserData.name)
      .should("not.exist");
  });
});
