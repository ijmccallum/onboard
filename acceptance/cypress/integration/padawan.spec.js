describe("Padawan crud", () => {
  beforeEach(() => {
    cy.server();
    cy.route("POST", "/api/**").as("api");
  });

  let email = "editest@wipro.com";
  let password = "test";

  //open user from list page
  //open uesr directly

  it("Create and remove a user", () => {
    const padawanUserData = {
      name: "padawan user",
      wiproemail: "padawan.user@wipro.com",
      password: "test"
    };

    cy.visit("/");
    cy.login({ email, password });
    cy.wait("@api");
    cy.visit("/#/users");

    //fill in the new user form
    cy.createUser(padawanUserData);
    cy.wait("@api");

    //go into the user's detail
    cy.get('[data-testid="user-list-item-name"]')
      .contains(padawanUserData.name)
      .click();

    //add them as our padawan
    cy.get('[data-testid="userobject-make-padawan-btn"]').click();

    //the add padawan button should be gone
    cy.get('[data-testid="userobject-make-padawan-btn"]').should("not.exist");

    //go to our user page
    cy.visit("/#/me");

    //the padawan should be there
    cy.get('[data-testid="padawan-list-item"]')
      .contains(padawanUserData.name)
      .should("exist");

    //reject them as a padawan
    cy.get('[data-testid="padawan-list-item"]')
      .contains(padawanUserData.name)
      .closest('[data-testid="padawan-list-item"]')
      .find('[data-testid="padawan-rejection-btn"]')
      .click();

    //they should be gone
    cy.get('[data-testid="padawan-list-item"]')
      .contains(padawanUserData.name)
      .should("not.exist");

    //go to their user page
    cy.visit("/#/users");
    cy.get('[data-testid="user-list-item-name"]')
      .contains(padawanUserData.name)
      .closest('[data-testid="user-list-item-name"]')
      .click();

    //the add padawan button should be there
    cy.get('[data-testid="userobject-make-padawan-btn"]').should("exist");

    //tidy up - delete the padawan user
    cy.get('[data-testid="userobject-delete-btn"]').click();
  });
});

//TODO: delete user, go to me, they should be gone - currently they're not
