Cypress.Commands.add("createUser", ({ name, wiproemail, password }) => {
  cy.get('[data-testid="new-user-name"]')
    .clear()
    .type(name)
    .should("have.value", name);
  cy.get('[data-testid="new-user-wiproemail"]')
    .clear()
    .type(wiproemail)
    .should("have.value", wiproemail);
  cy.get('[data-testid="new-user-password"]')
    .clear()
    .type(password)
    .should("have.value", password);

  cy.get('[data-testid="new-user-submit"]').click();
});
