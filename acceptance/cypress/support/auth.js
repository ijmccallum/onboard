Cypress.Commands.add("login", ({ email, password }) => {
  cy.visit("/");
  cy.get('[data-testid="login-email"]')
    .clear()
    .type(email)
    .should("have.value", email);
  cy.get('[data-testid="login-password"]')
    .clear()
    .type(password)
    .should("have.value", password);
  cy.get('[data-testid="login-submit"]').click();
});
