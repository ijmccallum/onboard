# onboard

Allaboard.

## Project todos

- cypress test delete user should be removed from padawan / buddy lists
- cypress test redirects
- crud a user's todos
- cypress test user todo cruds
- crud the boilerplate todos
- cypress test boilerplate todo cruds
- init user with todos
- show you as buddie for users you added as a padawan
- cypress test buddies showing up when somone is added as a padawan
- show progress bar on padawans
- get todos on another user's detail page
- crud your user details
- cypress test your user detail cruds
- add todo details / links
- ask buddy for help - show help requests from padawans, show your requests for help... threads?
- cypress test help requests
- https://www.npmjs.com/package/joi user input validation
- is currently connected to server flag - disconnected notice
- heroku!

## Onboarding todos.

For each thing:

- duration
  - Guess how long it took/will take
  - how long did it take you
  - how long has it taken other people
  - get an average!
- what day 1/2, does it depend on anything? Oh I see - we're making a gantt chart.
- detail page with chat/comments?

* All the docs, list of info you'll need
  - Previous addresses for the past 5/6 years
  - Previous employers for the past 5? years with reference & 'proof'
  - passport / gouvernment ID
  - p45/starter checklist (p60?)
* Names and faces
  - 'Talent Operations Partner'?(s)
  - Buddy(s)
  - Line manager
* Hardware
* Online places
  1.  Account / email set up
  - Slack
  - myWipro
    - personal details (myData app / personal details) - make sure it's all correct
    - Bank account details (myData app / bank details)
      - home salary: the account for your salary
      - home reimbursment: the account for any reimbursments to go into
      - payroll bank: just your NI number
    - line manager
    - asset allocation
    - mailing lists
  - Training
    - gdpr / data protection
    - fire thing
    - health thing?
  - Harvest
  - confluence
    - add your details to the team page
* Real life places
  - List the offices!? maybe?
