"use strict";
require("dotenv").config();
const app = require("./server/src/app");
// const logger = require("./logger");

const server = app.listen(process.env.PORT, err => {
  if (err) {
    console.log(`Failed to start server`, err);
    return;
  }
  console.log(`Server started on port ${process.env.PORT}`);
});

function stop() {
  server.close();
}

module.exports = server;
module.exports.stop = stop;
