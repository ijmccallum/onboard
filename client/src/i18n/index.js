import getLang from "./langDetector.js";
import axios from "axios";

const initI18n = function() {
  return new Promise(async (resolve, reject) => {
    //get the lang
    let lang = getLang();
    // lang = "es";

    //get the translation
    let translations;
    try {
      let res = await axios.get(`/api/locales/${lang}`);
      translations = res.data;
    } catch (err) {
      reject(err);
    }

    //function to access the translations
    window.t = function(tAddress) {
      let keys = tAddress.split(".");
      let drill = translations;
      keys.some(key => {
        if (!drill[key]) {
          console.warn("no translation provided for ", keys.join("."));
          drill = null;
          return true; //to break out the some
        }
        drill = drill[key];
      });
      return drill;
    };

    resolve();
  });
};

export default initI18n;
