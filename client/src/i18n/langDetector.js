const htmlTag = function() {
  let found;

  let htmlTag =
    typeof document !== "undefined" ? document.documentElement : null;
  if (htmlTag && typeof htmlTag.getAttribute === "function") {
    found = htmlTag.getAttribute("lang");
  }

  return found;
};

const navigator = function() {
  let found = [];

  if (typeof navigator !== "undefined") {
    if (navigator.languages) {
      // chrome only; not an array, so can't use .push.apply instead of iterating
      for (let i = 0; i < navigator.languages.length; i++) {
        found.push(navigator.languages[i]);
      }
    }
    if (navigator.userLanguage) {
      found.push(navigator.userLanguage);
    }
    if (navigator.language) {
      found.push(navigator.language);
    }
  }

  return found.length > 0 ? found : undefined;
};

const getLang = function() {
  let found;

  found = htmlTag();
  if (found) {
    console.log("html tag lang:", found);
    return found;
  }

  found = navigator();
  if (found) {
    console.log("navigator lang:", found);
    return found;
  }

  console.log("fallback lang:");

  return "en";
};
export default getLang;
