import { createSelector } from "reselect";

const todos = state => state.todos;

const todosRelevant = (state, props) => {
  if (props.myTodos) {
    return state.currentUser.userData;
  }
  return state.todos;
};

const todosSelector = createSelector(
  todosRelevant,
  todosState => todosState.todos
);

export { todosSelector };
