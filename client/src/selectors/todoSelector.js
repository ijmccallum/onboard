import { createSelector } from "reselect";

// testing code here to returning a single to do
const todo = (state, props) =>
  state.todos.todos.filter(td => td.label === props.todo.label)[0];

const todoNonRef = (state, props) => {
  let returnObj = {};
  let todos = [];
  if (props.myTodos) {
    todos = state.currentUser.userData.todos;
  } else {
    todos = state.todos.todos;
  }
  todos.forEach(td => {
    if (td._id == props.todo._id) {
      if (td.done == props.todo.done) {
        returnObj = props.todo;
      } else {
        returnObj = td;
      }
    }
  });
  return returnObj;
};

const todoSelector = createSelector(todoNonRef, todoState => todoState);

export { todoSelector, todo };
