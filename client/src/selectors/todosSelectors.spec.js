import {
  todosSelector,
  loadingSelector,
  errorSelector
} from "./todosSelectors";

describe("todos selector", () => {
  // test variables
  let thisTodos = [{ todo: "be cached", done: false }];
  const mockState = {
    todos: {
      todos: thisTodos,
      loading: false,
      error: false
    }
  };

  const mockNewState = {
    todos: {
      todos: [],
      loading: false,
      error: false
    }
  };

  it("should provide a todo array selector that returns todos from state", () => {
    expect(todosSelector(mockState, { myTodos: false })).toEqual(
      mockState.todos.todos
    );
  });
});
