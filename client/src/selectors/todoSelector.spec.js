import { todoSelector, todo } from "./todoSelector";

// test variables
const thisTodo = { label: "to be done", done: false };
const mockState = {
  todos: {
    todos: [thisTodo],
    loading: false,
    error: {}
  }
};

const mockNewState = {
  todos: {
    todos: [{ label: "to be done", done: false }],
    loading: false,
    error: {}
  }
};

describe("todo selector", () => {
  it("should provide the correct todo from state", () => {
    expect(todoSelector(mockState, { todo: thisTodo })).toEqual(thisTodo);
  });

  it("should return a new object reference only if the todo value changes", () => {
    let refTestObj = {
      label: "not ref test",
      done: false
    };
    const mockState = {
      todos: {
        todos: [refTestObj],
        loading: false,
        error: false
      }
    };

    let newRefTestObj = {
      label: "not ref test",
      done: true
    };
    const mockNewState = {
      todos: {
        todos: [newRefTestObj],
        loading: false,
        error: false
      }
    };

    const nonChangedTodo = todoSelector(mockState, { todo: refTestObj });
    const changedTodo = todoSelector(mockNewState, { todo: newRefTestObj });
    expect(nonChangedTodo).toBe(refTestObj);
    expect(changedTodo).not.toBe(refTestObj);
  });

  // code not used but testing anyway
  it("todo selector should return a single filtered todo", () => {
    const todoSelector = todo(mockState, { todo: thisTodo });
    expect(todoSelector).toEqual(thisTodo);
  });
});
