import {
  usersSelector,
  loadingSelector,
  errorSelector
} from "./usersSelector.js";

describe("users selector", () => {
  // test variables
  let thisusers = [{ user: "be cached", done: false }];
  const mockState = {
    users: {
      users: thisusers,
      loading: false,
      error: false
    }
  };

  const mockNewState = {
    users: {
      users: [],
      loading: false,
      error: false
    }
  };

  it("should provide a user array selector that returns users from state", () => {
    expect(usersSelector(mockState)).toEqual(mockState.users.users);
  });

  it("should provide a user loading selector that returns the user loading boolean from state", () => {
    expect(loadingSelector(mockState)).toEqual(mockState.users.loading);
  });

  it("should provide a user error selector that returns the user error from state", () => {
    expect(errorSelector(mockState)).toEqual(mockState.users.error);
  });
});
