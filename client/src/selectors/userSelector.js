import { createSelector } from "reselect";

// testing code here to returning a single to do
const user = (store, props) =>
  store.users.userss.filter(user => user._id === props.user._id)[0];

const userNonRef = (store, props) => {
  let userIdToSelect = props.match.params.userid;
  //check if it's the current user
  if (props.match.params.userid == store.currentUser.userData._id) {
    return store.currentUser.userData;
  }

  let returnObj = {};

  store.users.users.forEach(user => {
    if (user._id == userIdToSelect) {
      returnObj = user;
    }
  });

  //if there is a user list and no matching user was found
  if (store.users.users.length > 0 && !returnObj.wiproemail) {
    return "User missing";
  }

  return returnObj;
};

const userSelector = createSelector(userNonRef, userState => userState);

export { userSelector, user };
