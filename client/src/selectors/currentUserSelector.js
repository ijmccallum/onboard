import { createSelector } from "reselect";

const userId = (state, props) => {
  let userIdToReturn = "";
  if (props.myTodos) {
    userIdToReturn = state.currentUser.userData._id;
  }
  return userIdToReturn;
};

const userIdSelector = createSelector(userId, userIdState => userIdState);

export { userIdSelector };
