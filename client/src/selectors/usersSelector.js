import { createSelector } from "reselect";

const users = state => state.users;

const usersSelector = createSelector(users, usersState => usersState.users);
const loadingSelector = createSelector(users, usersState => usersState.loading);
const errorSelector = createSelector(users, usersState => usersState.error);

export { usersSelector, loadingSelector, errorSelector };
