import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import Header from "./components/Header/Header.js";
import Landing from "./components/Landing/Landing.js";
import MeLoader from "./components/Me/MeLoader.js";
import TodoListLoader from "./components/TodoList/TodoListLoader.js";
import { HashRouter as Router, Route, Redirect } from "react-router-dom";
import UsersLoader from "./components/Users/UsersLoader.js";

class App extends Component {
  constructor(props) {
    super(props);

    this.getUserData = this.getUserData.bind(this);
  }

  componentDidMount() {
    this.getUserData();
  }

  async getUserData() {
    try {
      const userDataRes = await axios.get("/api/me");
      this.props.setUserData(userDataRes.data.me);
      // set flag to render the app
      this.props.renderApp(true);
    } catch (error) {
      // set flag to render the app
      this.props.renderApp(true);
    }
  }

  render() {
    return (
      this.props.render && (
        <Router>
          <div>
            <Route path="/" component={Header} />
            <Route exact path="/" component={Landing} />
            <Route
              path="/me"
              component={() => {
                if (!this.props.isLoggedIn) {
                  return <Redirect to="/" />;
                }
                return <MeLoader />;
              }}
            />
            <Route
              path="/boilerplate-todos"
              component={() => {
                if (!this.props.isLoggedIn) {
                  return <Redirect to="/" />;
                }
                return <TodoListLoader myTodos={false} />;
              }}
            />
            <Route
              path="/users"
              component={() => {
                if (!this.props.isLoggedIn) {
                  return <Redirect to="/" />;
                }
                return <UsersLoader />;
              }}
            />
          </div>
        </Router>
      )
    );
  }
}

App.propTypes = {
  userData: PropTypes.shape({
    name: PropTypes.string,
    wiproemail: PropTypes.string,
    personalemail: PropTypes.string,
    todos: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string,
        done: PropTypes.bool
      })
    ),
    buddies: PropTypes.array,
    padawans: PropTypes.array
  }),
  setUserData: PropTypes.func.isRequired
};

export default App;
