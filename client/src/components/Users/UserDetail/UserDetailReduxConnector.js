import { connect } from "react-redux";
import { REMOVE_USER, SET_USERS } from "../../../store/users.js";
import {
  ADD_PADAWAN_TO_ME,
  REMOVE_PADAWAN_FROM_ME
} from "../../../store/currentUser.js";
import { userSelector } from "../../../selectors/userSelector.js";
import UserDetailDataProvider from "./UserDetailDataProvider.js";

//TODO: if me, get userdata from currentUser store
const mapStateToProps = (store, props) => ({
  user: userSelector(store, props),
  isMe: store.currentUser.userData._id == userSelector(store, props)._id,
  isMyPadawan: (() => {
    let thisUserId = userSelector(store, props)._id;
    let isIt = false;
    //TODO: short circuit this
    store.currentUser.userData.padawans.forEach(padawan => {
      if (padawan._id == thisUserId) {
        isIt = true;
      }
    });
    return isIt;
  })(),
  isMyBuddy: (() => {
    let thisUserId = userSelector(store, props)._id;
    let isIt = false;
    //TODO: short circuit this
    store.currentUser.userData.buddies.forEach(padawan => {
      if (padawan._id == thisUserId) {
        isIt = true;
      }
    });
    return isIt;
  })()
});

//TODO: change set users action to individual set user action and make the API call only for a single user

const mapDispatchToProps = dispatch => ({
  removeUser: user => dispatch(REMOVE_USER(user)),
  setUsers: users => dispatch(SET_USERS(users)),
  addPadawan: padawan => dispatch(ADD_PADAWAN_TO_ME(padawan)),
  removePadawan: padawan => dispatch(REMOVE_PADAWAN_FROM_ME(padawan))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDetailDataProvider);
