import React, { Component } from "react";
import PropTypes from "prop-types";
import UserIcon from "../../elements/Icons/User";
import SadDino from "../../elements/Icons/SadDino";
import Button from "../../elements/Button";
import PadawanIcon from "../../elements/Icons/Padawan.js";
import PadawanList from "../../PadawanList/PadawanList.js";

/**
 * User detail page
 */

class UserDetail extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    //TODO: current user detail, get data from currentUser store
    return (
      <div>
        <div className="flex items-start w-100 justify-between pa2 pt3 bb b--white-20">
          <div className="flex items-start">
            <UserIcon />
            <div className="flex flex-column ml2">
              <div data-testid="userobject-name">{this.props.user.name}</div>
              <div data-testid="userobject-wiproemail">
                {this.props.user.wiproemail}
              </div>
            </div>
          </div>
          <div className="flex">
            <Button
              className="button tc mr2"
              onClick={this.props.removeUser}
              data-testid="userobject-delete-btn"
            >
              <span className="mr2">Delete user</span> <SadDino />
            </Button>
            {!this.props.isMe &&
              !this.props.isMyBuddy &&
              !this.props.isMyPadawan && (
                <Button
                  onClick={this.props.makePadawan}
                  data-testid="userobject-make-padawan-btn"
                >
                  <span className="mr2">Make your padawan</span> <PadawanIcon />
                </Button>
              )}
          </div>
        </div>
        {/* <div className="pa2">List of user's todos...</div> */}
        <PadawanList
          padawans={this.props.user.padawans}
          rejectPadawan={this.props.rejectPadawan}
        />
      </div>
    );
  }
}

UserDetail.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    wiproemail: PropTypes.string.isRequired,
    padawans: PropTypes.array
  }).isRequired,
  isMe: PropTypes.bool.isRequired,
  isMyPadawan: PropTypes.bool.isRequired,
  isMyBuddy: PropTypes.bool.isRequired,
  removeUser: PropTypes.func.isRequired,
  makePadawan: PropTypes.func.isRequired,
  rejectPadawan: PropTypes.func.isRequired
};

export default UserDetail;
