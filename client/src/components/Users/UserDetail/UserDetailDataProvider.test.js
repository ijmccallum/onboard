import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import UserDetailDataProvider from "./UserDetailDataProvider.js";

import mockAxios from "axios";
jest.mock("axios");

describe("<UserDetailDataProvider />", function() {
  const userData = {
    _id: "lsdjf",
    name: "test user",
    wiproemail: "test.user@wipro.com"
  };

  it("On load it should call the API for user data", async function() {
    const setUsers = jest.fn(); //when this is called, set done
    await setUsers();
    shallow(
      <UserDetailDataProvider
        user={userData}
        setUsers={setUsers}
        addPadawan={() => {}}
        removeUser={() => {}}
        removePadawan={() => {}}
        isMe={false}
        isMyPadawan={false}
        isMyBuddy={false}
      />
    );
    expect(setUsers.mock.calls.length).toBe(1);
  });

  it("remove user should call the API and the removeUser prop function", async function() {
    const removeUser = jest.fn(); //when this is called, set done
    await removeUser();
    const userDP = shallow(
      <UserDetailDataProvider
        user={userData}
        removeUser={removeUser}
        addPadawan={() => {}}
        setUsers={() => {}}
        removePadawan={() => {}}
        isMe={false}
        isMyPadawan={false}
        isMyBuddy={false}
      />
    );
    userDP.instance().removeUser();
    expect(removeUser.mock.calls.length).toBe(1);
  });

  it("add padawan should call the API and the addPadawan prop function", async function() {
    const addPadawan = jest.fn(); //when this is called, set done
    await addPadawan();
    const userDP = shallow(
      <UserDetailDataProvider
        user={userData}
        removeUser={() => {}}
        addPadawan={addPadawan}
        setUsers={() => {}}
        removePadawan={() => {}}
        isMe={false}
        isMyPadawan={false}
        isMyBuddy={false}
      />
    );
    userDP.instance().makePadawan(); //make padawan calls the API, then the addPadawan when the api returns
    expect(addPadawan.mock.calls.length).toBe(1);
  });

  //if user="User missing", it should return a redirect component
});
