import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import UserDetail from "./UserDetail.js";
import axios from "axios";

/**
 * Get the user's data
 */

class UserDetailDataProvider extends Component {
  constructor(props) {
    super(props);
    let loading = false;
    if (!props.user._id) {
      this.getUsersData();
      loading = true;
    }
    this.state = {
      loading: loading
    };

    this.removeUser = this.removeUser.bind(this);
    this.makePadawan = this.makePadawan.bind(this);
    this.rejectPadawan = this.rejectPadawan.bind(this);
  }

  async getUsersData() {
    const usersDataRes = await axios.get("/api/users");
    this.props.setUsers(usersDataRes.data.users);
    this.setState({
      loading: false
    });
  }

  async removeUser() {
    try {
      const removeRes = await axios.delete("/api/users", {
        data: {
          _id: this.props.user._id
        }
      });

      this.props.removeUser(this.props.user);
    } catch (error) {
      console.error("remove user err:R", error);
    }
  }

  async makePadawan() {
    //to make this user your padawan
    try {
      //TODO: this route
      const newPadawanRes = await axios.put("/api/me/padawans", {
        data: {
          _id: this.props.user._id
        }
      });

      this.props.addPadawan(this.props.user); //TODO: this
    } catch (error) {
      console.error("Make padawan error", error);
    }
  }

  async rejectPadawan(padawan) {
    //to make this user your padawan
    try {
      const rejectedPadawanRes = await axios.delete("/api/me/padawans", {
        data: {
          _id: padawan._id
        }
      });

      this.props.removePadawan(padawan); //TODO: this
    } catch (error) {
      console.error("Make padawan error", error);
    }
  }

  render() {
    if (this.props.user == "User missing") {
      return <Redirect to="/users" />;
    }
    if (this.state.loading) {
      return <p>Loading user data...</p>;
    }
    return (
      <UserDetail
        {...this.props}
        removeUser={this.removeUser}
        makePadawan={this.makePadawan}
        rejectPadawan={this.rejectPadawan}
      />
    );
  }
}

//TODO: change to just be _id then load the user data for that given _id
UserDetailDataProvider.propTypes = {
  user: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
      _id: PropTypes.string,
      name: PropTypes.string,
      wiproemail: PropTypes.string
    })
  ]),
  removeUser: PropTypes.func.isRequired,
  addPadawan: PropTypes.func.isRequired,
  removePadawan: PropTypes.func.isRequired,
  setUsers: PropTypes.func.isRequired
};

export default UserDetailDataProvider;
