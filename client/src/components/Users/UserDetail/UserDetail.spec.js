import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import UserDetail from "./UserDetail.js";

describe("<UserDetail />", () => {
  const userData = {
    _id: "asd",
    name: "test name",
    wiproemail: "test@wipro.com"
  };

  it("should render a user component", () => {
    //Not really muich to test here unless we figure out how to mock rect router?
    const usersComponent = shallow(
      <UserDetail
        removeUser={() => {}}
        makePadawan={() => {}}
        rejectPadawan={() => {}}
        user={userData}
        isMe={false}
        isMyPadawan={false}
        isMyBuddy={false}
      />
    );

    expect(usersComponent.length).toBe(1);
  });

  it("should include a delete button that calles the passed in removeUser function", () => {
    const removeFunction = jest.fn();
    const usersComponent = shallow(
      <UserDetail
        removeUser={removeFunction}
        makePadawan={() => {}}
        rejectPadawan={() => {}}
        user={userData}
        isMe={false}
        isMyPadawan={false}
        isMyBuddy={false}
      />
    );
    usersComponent
      .find('[data-testid="userobject-delete-btn"]')
      .simulate("click");

    expect(removeFunction.mock.calls.length).toBe(1);
  });

  it("should include a make padawan button that calles the passed in makePadawan function", () => {
    const makePadawanFunction = jest.fn();
    const usersComponent = shallow(
      <UserDetail
        removeUser={() => {}}
        makePadawan={makePadawanFunction}
        rejectPadawan={() => {}}
        user={userData}
        isMe={false}
        isMyPadawan={false}
        isMyBuddy={false}
      />
    );
    usersComponent
      .find('[data-testid="userobject-make-padawan-btn"]')
      .simulate("click");

    expect(makePadawanFunction.mock.calls.length).toBe(1);
  });
});
