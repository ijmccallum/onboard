import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import UsersList from "./UsersList.js";

class UsersDataProvider extends Component {
  constructor(props) {
    super(props);
    let loading = false;

    if (this.props.users.length == 0) {
      this.getUsersData();
      loading = true;
    }

    this.state = {
      loading: loading
    };
  }

  async getUsersData() {
    const usersDataRes = await axios.get("/api/users");
    this.props.setUsers(usersDataRes.data.users);
    this.setState({
      loading: false
    });
  }

  render() {
    if (this.state.loading) {
      return <p>Loading users...</p>;
    }
    return (
      <UsersList
        users={this.props.users}
        currentUser={this.props.currentUser}
      />
    );
  }
}

UsersDataProvider.propTypes = {
  users: PropTypes.array.isRequired,
  setUsers: PropTypes.func.isRequired
};

export default UsersDataProvider;
