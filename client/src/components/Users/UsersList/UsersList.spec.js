import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import UsersList from "./UsersList.js";

describe("<UsersList />", () => {
  const testUserList = [
    {
      _id: "lsdkfjlsdkfj",
      name: "user one",
      wiproemail: "user.one@wipro.com",
      personalemail: "random@hello.com"
    },
    {
      _id: "lsdkfjlsddsf",
      name: "user two",
      wiproemail: "user.two@wipro.com",
      personalemail: "hello@random.com"
    }
  ];
  it("should render a list of given users", () => {
    const usersListComponent = shallow(
      <UsersList
        users={testUserList}
        currentUser={{
          _id: "asdasd",
          name: "test name",
          wiproemail: "test@wipro.com",
          personalemail: "me@you.com"
        }}
        loading={false}
      />
    );

    //TODO: make this test a bit more robust... although it's not really doing much of anything.
    expect(
      usersListComponent.find('[data-testid="users-list-item-wrap"]').length
    ).toBe(2);
  });
});
