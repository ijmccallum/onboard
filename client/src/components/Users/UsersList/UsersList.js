import React, { Component } from "react";
import PropTypes from "prop-types";
import UserListItem from "./UserListItem/UserListItem.js";

class UsersList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.users.length == 0) {
      return <p>No users.</p>;
    }

    let currentUser = this.props.currentUser;
    console.log("UserList.js current user", this.props.currentUser);
    return (
      <ul className="pa0 ma0">
        {this.props.users.map(user => (
          <UserListItem
            data-testid="users-list-item-wrap"
            key={user._id}
            userObject={user}
            isMe={currentUser && currentUser._id == user._id}
            isMyBuddy={false}
            isMyPadawan={false}
          />
        ))}
      </ul>
    );
  }
}

UsersList.propTypes = {
  users: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      name: PropTypes.string,
      wiproemail: PropTypes.string,
      personalemail: PropTypes.string
    })
  ),
  currentUser: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string,
    wiproemail: PropTypes.string,
    personalemail: PropTypes.string
  })
};

export default UsersList;
