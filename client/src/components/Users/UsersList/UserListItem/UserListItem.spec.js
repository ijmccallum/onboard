import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import UserListItem from "./UserListItem.js";

describe("<UserListItem />", () => {
  const testUser = {
    _id: "lsdkfjlsdkfj",
    name: "user one",
    wiproemail: "user.one@wipro.com",
    personalemail: "random@hello.com"
  };
  it("should render a list item for the given user", () => {
    const userListItemComponent = shallow(
      <UserListItem
        userObject={testUser}
        isMe={false}
        isMyBuddy={false}
        isMyPadawan={false}
      />
    );

    expect(
      userListItemComponent.find('[data-testid="user-list-item-name"]').text()
    ).toBe("user one");

    const compLink = userListItemComponent
      .find('[data-testid="user-list-item-link"]')
      .props().to;
    expect(compLink.includes("lsdkfjlsdkfj")).toBe(true);
  });
});
