import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import UserIcon from "../../../elements/Icons/User";
import { ListItem } from "../../../blocks/ListItem";
import Button from "../../../elements/Button";
import PadawanIcon from "../../../elements/Icons/Padawan.js";
import StudentIcon from "../../../elements/Icons/Student.js";
import KingIcon from "../../../elements/Icons/King.js";
/**
 * Preview of a user
 */

class UserListItem extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let userUrl = `/users/${this.props.userObject._id}`;
    if (this.props.isMe) {
      userUrl = "/me";
    }
    return (
      <ListItem>
        <Link
          to={userUrl}
          data-testid="user-list-item-link"
          className="no-underline flex items-center pa2 f6"
        >
          {this.props.isMe && <KingIcon />}
          {!this.props.isMe && <UserIcon />}

          <div data-testid="user-list-item-name" className="mh2">
            {this.props.userObject.name}
          </div>
        </Link>
      </ListItem>
    );
  }
}

UserListItem.propTypes = {
  userObject: PropTypes.shape({
    name: PropTypes.string.isRequired,
    wiproemail: PropTypes.string.isRequired,
    personalemail: PropTypes.string,
    _id: PropTypes.string.isRequired
  }),
  isMe: PropTypes.bool.isRequired,
  isMyPadawan: PropTypes.bool.isRequired,
  isMyBuddy: PropTypes.bool.isRequired
};

export default UserListItem;
