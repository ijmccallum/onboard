import { connect } from "react-redux";
import UsersListDataProvider from "./UsersListDataProvider.js";

import { SET_USERS } from "../../../store/users.js";

import { usersSelector } from "../../../selectors/usersSelector.js";

const mapStateToProps = store => ({
  users: usersSelector(store),
  currentUser: store.currentUser.userData
});

const mapDispatchToProps = dispatch => ({
  setUsers: users => dispatch(SET_USERS(users))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersListDataProvider);
