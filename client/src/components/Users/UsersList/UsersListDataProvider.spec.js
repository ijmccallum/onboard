import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import UsersListDataProvider from "./UsersListDataProvider.js";

import mockAxios from "axios";
jest.mock("axios");

describe("<UsersListDataProvider />", function() {
  it("Should get users from the api", async function() {
    const setUsersFn = jest.fn(); //when this is called, set done
    await setUsersFn();
    shallow(<UsersListDataProvider setUsers={setUsersFn} users={[]} />);
    expect(setUsersFn.mock.calls.length).toBe(1);
  });
});
