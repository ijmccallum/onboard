import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import NewUserForm from "./NewUserForm.js";
const propsForDumbComp = {
  values: {
    name: "test name",
    wiproemail: "test.name@wipro.com",
    password: "password"
  },
  errors: {},
  touched: {},
  handleChange: () => {},
  handleBlur: () => {},
  handleSubmit: () => {},
  loading: false,
  serverror: false
};

describe("<NewUserForm />", function() {
  it("Should provide an add user function that will call the server", async function() {
    const component = shallow(<NewUserForm {...propsForDumbComp} />);
    expect(component.length).toBe(1);
  });

  //TODO: clicking submit calls the passed in handle submit
  //TODO: passing in touched & error shows the error for each field
});
