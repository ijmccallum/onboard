import React, { Component } from "react";
import PropTypes from "prop-types";
import NewUserFormik from "./NewUserFormik.js";
import axios from "axios";

/**
 * Talk to the API because the hand of the king was shot through by an arrow
 */

class NewUserDataProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      serverror: false
    };
    this.addUser = this.addUser.bind(this);
  }

  async addUser({ wiproemail, name, password }) {
    try {
      const response = await axios.post("/api/users", {
        wiproemail,
        name,
        password
      });

      this.props.addUser(response.data);
    } catch (error) {
      console.error(
        "Users/NewUser/NewUserDataProvider.js add user error: ",
        error
      );
      this.setState({
        serverror: error.response.data.err
      });
    }
  }

  render() {
    return (
      <NewUserFormik
        {...this.props}
        onSubmit={this.addUser}
        loading={this.state.loading}
        serverror={this.state.serverror}
      />
    );
  }
}

NewUserDataProvider.propTypes = {
  addUser: PropTypes.func.isRequired,
  users: PropTypes.array.isRequired
};

export default NewUserDataProvider;
