import { connect } from "react-redux";
import { ADD_USER } from "../../../store/users.js";
import { usersSelector } from "../../../selectors/usersSelector.js";
import NewUserDataProvider from "./NewUserDataProvider.js";

const mapStateToProps = store => ({
  users: usersSelector(store)
});

const mapDispatchToProps = dispatch => ({
  addUser: userData => {
    if (userData) {
      dispatch(ADD_USER(userData));
    }
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewUserDataProvider);
