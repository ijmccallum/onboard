import React, { Component } from "react";
import PropTypes from "prop-types";
import { Formik } from "formik";
import NewUserForm from "./NewUserForm.js";

class NewUserFormik extends Component {
  constructor(props) {
    super(props);

    this.validateValues = this.validateValues.bind(this);
    this.submissionHandler = this.submissionHandler.bind(this);
  }

  validateValues(values, props) {
    //TODO: more validation
    const errors = {};
    if (!values.wiproemail) {
      errors.wiproemail = "Required";
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.wiproemail)
    ) {
      errors.wiproemail = "Invalid email address";
    }
    if (!values.name) {
      errors.name = "Required";
    }
    if (!values.password) {
      errors.password = "Required";
    }
    return errors;
  }

  /* gets setValues, setStatus, and other goodies */
  submissionHandler(formValues) {
    this.props.onSubmit({
      wiproemail: formValues.wiproemail,
      name: formValues.name,
      password: formValues.password
    });
  }

  render() {
    return (
      <Formik
        initialValues={{
          wiproemail: "@wipro.com",
          name: "",
          password: "test"
        }}
        validate={this.validateValues}
        onSubmit={this.submissionHandler}
        render={props => {
          return <NewUserForm {...this.props} {...props} />;
        }}
      />
    );
  }
}

NewUserFormik.propTypes = {
  onSubmit: PropTypes.func.isRequired
};
export default NewUserFormik;
