import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import NewUserDataProvider from "./NewUserDataProvider.js";
const propsForDumbComp = {
  title: "test",
  loading: false,
  error: {
    userError: "",
    systemError: ""
  },
  todos: []
};
import mockAxios from "axios";
jest.mock("axios");

describe("<NewUserDataProvider />", function() {
  it("Should provide an add user function that will call the server", async function() {
    mockAxios.post.mockImplementation(() => {
      return Promise.resolve({
        data: "hi"
      });
    });
    const addUserFn = jest.fn();
    // await addUserFn();
    const component = shallow(
      <NewUserDataProvider addUser={addUserFn} users={[]} />
    );
    await component.instance().addUser({
      wiproemail: "wiproemail",
      name: "name",
      password: "password"
    });
    expect(addUserFn.mock.calls.length).toBe(1);
  });

  it("Should handle errors returned by the server", async function() {
    mockAxios.post.mockImplementation(() => {
      return Promise.reject({
        response: {
          data: {
            err: "Test breakage"
          }
        }
      });
    });
    const addUserFn = jest.fn();
    // await addUserFn();
    const component = shallow(
      <NewUserDataProvider addUser={addUserFn} users={[]} />
    );
    await component.instance().addUser({
      wiproemail: "wiproemail",
      name: "name",
      password: "password"
    });
    expect(addUserFn.mock.calls.length).toBe(0);
  });
});
