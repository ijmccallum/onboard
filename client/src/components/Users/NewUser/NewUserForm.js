import React from "react";
import PropTypes from "prop-types";
import Input from "../../elements/Input";
import Button from "../../elements/Button";

// Our inner form component which receives our form's state and updater methods as props
const NewUserForm = ({
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  handleSubmit,
  loading,
  serverror
}) => (
  <form onSubmit={handleSubmit} className="pa2">
    <h2 className="f5">Create new user.</h2>
    <Input.Wrapper>
      <Input.Label>Name:</Input.Label>
      <Input.Input
        type="text"
        name="name"
        id="new-user-name"
        data-testid="new-user-name"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.name}
      />
      <Input.Helper>
        {touched.name && errors.name && <div>{errors.name}</div>}
      </Input.Helper>
    </Input.Wrapper>

    <Input.Wrapper>
      <Input.Label>Wipro email:</Input.Label>
      <Input.Input
        type="email"
        name="wiproemail"
        id="new-user-wiproemail"
        data-testid="new-user-wiproemail"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.wiproemail}
      />
      <Input.Helper>
        {touched.wiproemail &&
          errors.wiproemail && <div>{errors.wiproemail}</div>}
      </Input.Helper>
    </Input.Wrapper>

    <Input.Wrapper>
      <Input.Label>Password:</Input.Label>
      <Input.Input
        type="password"
        name="password"
        data-testid="new-user-password"
        id="new-user-password"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.password}
      />
      <Input.Helper>
        {touched.password && errors.password && <div>{errors.password}</div>}
      </Input.Helper>
    </Input.Wrapper>

    {serverror && <p data-testid="new-user-serverror">{serverror}</p>}
    <div className="pv2">
      <Button type="submit" disabled={loading} data-testid="new-user-submit">
        Submit
      </Button>
    </div>
  </form>
);

NewUserForm.propTypes = {
  values: PropTypes.shape({
    name: PropTypes.string.isRequired,
    wiproemail: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired
  }),
  errors: PropTypes.shape({
    name: PropTypes.string,
    wiproemail: PropTypes.string,
    password: PropTypes.string
  }),
  touched: PropTypes.shape({
    name: PropTypes.bool,
    wiproemail: PropTypes.bool,
    password: PropTypes.bool
  }),
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  serverror: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
};

export default NewUserForm;
