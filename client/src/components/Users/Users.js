import React, { Component } from "react";
import { Route } from "react-router-dom";
import UsersListReduxConnector from "./UsersList/UsersListReduxConnector.js";
import NewUserReduxConnector from "./NewUser/NewUserReduxConnector";
import UserDetailReduxConnector from "./UserDetail/UserDetailReduxConnector";
import Page from "../blocks/Page";

class Users extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Route
          path="/users"
          exact
          render={() => {
            return (
              <Page.Wrapper>
                <Page.Title>Users</Page.Title>
                <Page.Body>
                  <UsersListReduxConnector />
                  <NewUserReduxConnector />
                </Page.Body>
              </Page.Wrapper>
            );
          }}
        />
        <Route path="/users/:userid" component={UserDetailReduxConnector} />
      </div>
    );
  }
}

Users.propTypes = {};

export default Users;
