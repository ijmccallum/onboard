import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import Users from "./Users.js";

describe("<Users />", () => {
  it("should render a user component", () => {
    //Not really muich to test here unless we figure out how to mock rect router?
    const usersComponent = shallow(<Users />);

    expect(usersComponent.length).toBe(1);
  });
});
