import React, { Component } from "react";
import Loadable from "react-loadable";

const Loader = Loadable({
  loader: () => import("./Users.js"),
  loading() {
    return <p>loading...</p>;
  }
});

class UsersLoader extends Component {
  render() {
    return <Loader />;
  }
}

export default UsersLoader;
