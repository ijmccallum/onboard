import styled from "styled-components";

const ButtonDelete = styled.button`
  background-color: black;
  display: contents;

  svg {
    width: 1.1rem;
  }
  svg:hover {
    background-color: #00a3e6;
  }
`;

export default ButtonDelete;
