import styled from "styled-components";

const Wrapper = styled.div`
  margin-right: 5px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  ${props => {
    if (props.done) {
      return "text-decoration: line-through; color: #999;";
    }
  }};
`;

const Label = styled.label`
  width: 100%;
  padding: 0.5rem;
`;

const Input = styled.input`
  border-style: none;
  border-left: solid 5px;
  border-left-color: ${props => {
    if (props.colour) {
      return props.colour;
    }
    if (props.type == "email") {
      return "#ffdb00";
    }
    if (props.type == "password") {
      return "#ff0037";
    }
    if (props.type == "text") {
      return "#3fc631";
    }
  }};
  padding: 8px;
  margin-left: 8px;
`;

const Helper = styled.div`
  font-style: italic;
`;

export default {
  Wrapper,
  Label,
  Input,
  Helper
};
