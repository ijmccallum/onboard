import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import PunchTheAir from "./punchTheAir.js";
import ThrowIt from "./throwIt.js";
import Jumper from "./jumper.js";
import Flyer from "./flyer.js";
import Kicker from "./kicker.js";
import Diver from "./diver.js";
import HeadButter from "./headButter.js";
import Star from "./star.js";
import Splitter from "./splitter.js";
import UserIcon from "./index.js";

describe("<PunchTheAir />", () => {
  it("should render a react component", () => {
    const inputComponent = shallow(<PunchTheAir />);

    expect(inputComponent.length).toBe(1);
  });
});

describe("<ThrowIt />", () => {
  it("should render a react component", () => {
    const inputComponent = shallow(<ThrowIt />);

    expect(inputComponent.length).toBe(1);
  });
});

describe("<Jumper />", () => {
  it("should render a react component", () => {
    const inputComponent = shallow(<Jumper />);

    expect(inputComponent.length).toBe(1);
  });
});

describe("<Flyer />", () => {
  it("should render a react component", () => {
    const inputComponent = shallow(<Flyer />);

    expect(inputComponent.length).toBe(1);
  });
});

describe("<Kicker />", () => {
  it("should render a react component", () => {
    const inputComponent = shallow(<Kicker />);

    expect(inputComponent.length).toBe(1);
  });
});

describe("<Diver />", () => {
  it("should render a react component", () => {
    const inputComponent = shallow(<Diver />);

    expect(inputComponent.length).toBe(1);
  });
});
describe("<HeadButter />", () => {
  it("should render a react component", () => {
    const inputComponent = shallow(<HeadButter />);

    expect(inputComponent.length).toBe(1);
  });
});
describe("<Star />", () => {
  it("should render a react component", () => {
    const inputComponent = shallow(<Star />);

    expect(inputComponent.length).toBe(1);
  });
});
describe("<Splitter />", () => {
  it("should render a react component", () => {
    const inputComponent = shallow(<Splitter />);

    expect(inputComponent.length).toBe(1);
  });
});
describe("<UserIcon />", () => {
  it("should render one of the icons", () => {
    const inputComponent = shallow(<UserIcon />);

    expect(inputComponent.length).toBe(1);
  });
});
