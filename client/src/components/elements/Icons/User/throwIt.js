import React, { Component } from "react";
import PropTypes from "prop-types";

/**
 * YEAH
 */

const ThrowIt = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 58.301 58.301"
      className="db w2"
    >
      <circle cx="33.597" cy="8.283" r="5.515" fill="#FFFFFF" />
      <path
        d="M44.377,10.429c-1.397-0.539-2.968,0.155-3.506,1.552l-2.002,5.182l-9.319-2.466    C25.527,9.173,20.044,3.929,16.846,0.78c-1.066-1.051-2.784-1.038-3.833,0.029c-1.052,1.067-1.039,2.783,0.028,3.834    c5.164,5.087,11.125,11.45,12.142,13.244l0.071,2.155c-1.095,6.278-8.131,18.601-8.131,18.601s1.704,0.909,3.825,1.691    c-2.709,3.569-5.406,7.153-8.159,10.691c-2.27,2.92,2.491,6.097,4.738,3.204c3.093-3.977,6.116-8.01,9.165-12.013    c2.291,0.64,4.722,1.185,6.946,1.401c1.317,3.666,1.896,7.555,1.646,11.554c-0.228,3.662,5.444,4.385,5.676,0.699    c0.28-4.475-0.258-8.775-1.555-12.862c0.685-0.33,1.246-0.785,1.628-1.4c0,0-4.373-13.292-4.64-19.492l3.436,0.909    c0.231,0.061,0.465,0.09,0.694,0.09c1.096,0,2.116-0.669,2.527-1.734l2.878-7.447C46.467,12.539,45.772,10.969,44.377,10.429z"
        fill="#FFFFFF"
      />
    </svg>
  );
};

ThrowIt.propTypes = {
  fill: PropTypes.string
};

export default ThrowIt;
