import React, { Component } from "react";
import PropTypes from "prop-types";
import PunchTheAir from "./punchTheAir.js";
import ThrowIt from "./throwIt.js";
import Jumper from "./jumper.js";
import Flyer from "./flyer.js";
import Kicker from "./kicker.js";
import Diver from "./diver.js";
import HeadButter from "./headButter.js";
import Star from "./star.js";
import Splitter from "./splitter.js";

/**
 * YEAH
 */

const UserIcon = props => {
  const potential = [
    PunchTheAir,
    ThrowIt,
    Jumper,
    Flyer,
    Kicker,
    Diver,
    HeadButter,
    Star,
    Splitter
  ];
  const randomIndex = Math.floor(Math.random() * potential.length);
  const Return = potential[randomIndex];

  return <Return fill={props.fill} />;
};

UserIcon.propTypes = {
  fill: PropTypes.string
};

export default UserIcon;
