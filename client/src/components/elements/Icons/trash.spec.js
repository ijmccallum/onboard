import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import Trash from "./trash.js";

describe("<Trash />", () => {
  it("should render one of the icons", () => {
    const inputComponent = shallow(<Trash />);

    expect(inputComponent.length).toBe(1);
  });
});
