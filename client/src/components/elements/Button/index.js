import styled from "styled-components";

const Button = styled.button`
  ${props => {
    if (props.transparent) {
      return "background-color: transparent;";
    } else {
      return "background-color: black;";
    }
  }} color: white;
  border-style: none;
  padding: 8px;
  border-radius: 2px;
  display: flex;
  align-items: center;

  :hover {
    background-color: #00a3e6;
  }
`;

export default Button;
