import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import Input from "./index.js";

//TODO: update these to test the Input.Thing updates
describe("<Input />", () => {
  it("should render a react component", () => {
    const inputComponent = shallow(<Input.Input colour="#fff" />);

    expect(inputComponent.length).toBe(1);
  });

  it("should render an email component", () => {
    const inputComponent = shallow(<Input.Input type="email" />);

    expect(inputComponent.length).toBe(1);
  });
  it("should render a password component", () => {
    const inputComponent = shallow(<Input.Input type="password" />);

    expect(inputComponent.length).toBe(1);
  });

  it("should render a text component", () => {
    const inputComponent = shallow(<Input.Input type="text" />);

    expect(inputComponent.length).toBe(1);
  });
});
