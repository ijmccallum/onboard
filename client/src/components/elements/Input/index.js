import styled from "styled-components";

const Wrapper = styled.div`
  margin: 1rem 0.5rem 1rem 0;
`;

const Label = styled.label`
  display: block;
  margin-bottom: 0.5em;
`;

const Input = styled.input`
  border-style: none;
  border-left: solid 5px;
  border-left-color: ${props => {
    if (props.colour) {
      return props.colour;
    }
    if (props.type == "email") {
      return "#ffdb00";
    }
    if (props.type == "password") {
      return "#ff0037";
    }
    if (props.type == "text") {
      return "#3fc631";
    }
  }};
  padding: 8px;
`;

const Helper = styled.div`
  font-style: italic;
`;

export default {
  Wrapper,
  Label,
  Input,
  Helper
};
