import styled from "styled-components";

const TodoAddWrapper = styled.div`
  display: flex;
  margin-bottom: 10px;
  margin-left: 8px;

  button {
    margin-left: 8px;
  }
`;

export default TodoAddWrapper;
