import styled from "styled-components";

//<li> with borders that shows a grey bg on hover

const ListItem = styled.li`
  width: 100%;
  border-top: 1px solid #666;
  border-bottom: 1px solid #666;
  margin-top: -1px;
  display: flex;
  justify-content: space-between;

  :hover {
    background-color: #444;
  }
`;

export { ListItem };
