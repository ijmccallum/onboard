import styled from "styled-components";

const Wrapper = styled.div`
  border-radius: 5px;
  padding: 1rem 0;
  background-color: rgba(20, 20, 20, 0.5);
`;

const Title = styled.h2`
  font-size: 1rem;
  margin: 0 1rem 1rem;
`;

export default { Wrapper, Title };
