import styled from "styled-components";

const Wrapper = styled.div`
  padding-bottom: 2rem;
`;

const Title = styled.h1`
  font-size: 1.5rem;
  padding: 1rem 0.5rem;
  margin: 0;
`;

const Body = styled.div``;
export default { Wrapper, Title, Body };
