import React, { Component } from "react";
import axios from "axios";
import Button from "../../elements/Button";
import Input from "../../elements/Input";
import TodoAddWrapper from "../../elements/TodoAddWrapper";

class TodoAdd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: ""
    };
    this.addTodo = this.addTodo.bind(this);
  }

  render() {
    return (
      <TodoAddWrapper>
        <Input.Input
          type="text"
          placeholder="Enter new todo"
          onChange={event => this.onInputChange(event.target.value)}
        />
        <Button
          transparent={false}
          onClick={() => this.addTodo(this.state.inputValue)}
        >
          Add Todo
        </Button>
      </TodoAddWrapper>
    );
  }

  onInputChange(value) {
    this.setState({ inputValue: value });
  }

  async addTodo(newTodoLabel) {
    try {
      /** only add if value exists */
      if (newTodoLabel === "") {
        return;
      }
      /** only do this for myTodos for now */
      if (this.props.myTodos) {
        /** post new todo rest call */
        const postResponse = await axios.post("/api/users/todos/todo", {
          data: {
            userId: this.props.userId,
            newTodo: {
              label: newTodoLabel,
              done: false
            }
          }
        });
        /** parse string json response to json object */
        const responseTodoJson = JSON.parse(postResponse.request.response);
        /** new id assigned from response */
        const newTodoId = responseTodoJson._id;
        /** newTodo object for the store */
        const newTodo = {
          _id: newTodoId,
          label: newTodoLabel,
          done: false
        };
        /** add newtodo to store */
        this.props.addTodo(newTodo);
      }
    } catch (error) {
      console.error(
        error,
        "There has been an error adding a new todo, please contact your administrator."
      );
    }
  }
}

export default TodoAdd;
