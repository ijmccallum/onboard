import { connect } from "react-redux";
import { ADD_MYTODO } from "../../../store/currentUser.js";
import TodoAdd from "./TodoAdd";

const mapStateToProps = store => ({});

const mapDispatchToProps = dispatch => ({
  addTodo: newTodo => dispatch(ADD_MYTODO(newTodo))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoAdd);
