import React from "react";
import PropTypes from "prop-types";
import TodoReduxConnector from "./Todo/TodoReduxConnector.js";

const TodoList = function({ title, todos, myTodos, userId }) {
  if (!todos || todos.length === 0) {
    return <p className="ph3">No todos... yet</p>;
  }

  return (
    <ul className="list ma0 pa0">
      {todos.map((todo, i) => (
        <TodoReduxConnector
          myTodos={myTodos}
          todo={todo}
          key={todo._id}
          data-testid="todolist-li"
          userId={userId}
        />
      ))}
    </ul>
  );
};

TodoList.propTypes = {
  todos: PropTypes.array,
  userId: PropTypes.string,
  myTodos: PropTypes.bool.isRequired,
  userId: PropTypes.string.isRequired
};

export default TodoList;
