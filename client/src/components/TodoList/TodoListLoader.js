import React, { Component } from "react";
import Loadable from "react-loadable";

const Loader = Loadable({
  loader: () => import("./TodoListReduxConnector.js"),
  loading() {
    return <p>loading...</p>;
  }
});

class TodoListLoader extends Component {
  render() {
    return <Loader {...this.props} />;
  }
}

export default TodoListLoader;
