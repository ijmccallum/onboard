import React from "react";
import PropTypes from "prop-types";
import Checkbox from "../../elements/Checkbox";
import { ListItem } from "../../blocks/ListItem";
import Trash from "../../elements/Icons/trash";
import ButtonDelete from "../../elements/ButtonDelete";

const Todo = function({ todo, setTodoDone, removeTodo, myTodos }) {
  return (
    <ListItem done={todo.done}>
      <Checkbox.Wrapper done={todo.done}>
        <Checkbox.Input
          className="todo__checkbox"
          type="checkbox"
          checked={todo.done}
          onChange={() => setTodoDone(todo, !todo.done)}
          id={`todo${todo._id}`}
        />
        <Checkbox.Label
          className="todo__label f6"
          data-testid="todoText"
          htmlFor={`todo${todo._id}`}
        >
          {todo.label}
        </Checkbox.Label>
      </Checkbox.Wrapper>
      {myTodos && (
        <Checkbox.Wrapper>
          <ButtonDelete onClick={() => removeTodo(todo)}>
            <Trash />
          </ButtonDelete>
        </Checkbox.Wrapper>
      )}
    </ListItem>
  );
};

Todo.propTypes = {
  todo: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    done: PropTypes.bool.isRequired
  }),
  setTodoDone: PropTypes.func.isRequired
};

export default Todo;
