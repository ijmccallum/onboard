import React from "react";
import { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import mockAxios from "axios";
import TodoDataProvider from "./TodoDataProvider";
configure({ adapter: new Adapter() });

describe("<TodoDataProvider />", () => {
  it("Should call api via axios if 'myTodos' prop is true", async () => {
    /** mock jest function to mock the action being passed in as a prop */
    const toggleCheckboxFunction = jest.fn();
    /** shallow get the component with expected props, 'myTodo' is set to true for this test */
    const todoDataProvider = shallow(
      <TodoDataProvider
        todo={{
          _id: "idTestWithNumbers012345",
          label: "Wipro background check",
          done: false
        }}
        toggleCheckboxChange={toggleCheckboxFunction}
        myTodos={true}
      />
    );
    /** mock todo object for mock parameter */
    const mocTodo = {
      _id: "idTestWithNumbers012345",
      label: "Wipro background check",
      done: false
    };
    /** call the function that should call the put todo api because 'myTodos' is true */
    await todoDataProvider.instance().setTodoDone(mocTodo, false);
    /** expect the function to be called once */
    expect(mockAxios.put).toHaveBeenCalledTimes(1);
    /** expect the action function to also be called */
    expect(toggleCheckboxFunction).toHaveBeenCalledTimes(1);
    /** clear mock axios calls before next test is run */
    jest.clearAllMocks();
  });

  it("should not call axios if 'myTodo' is false", async () => {
    /** mock jest function to mock the action being passed in as a prop */
    const toggleCheckboxFunction = jest.fn();
    /** shallow get the component with expected props, 'myTodo' is set to true for this test */
    const todoDataProvider2 = shallow(
      <TodoDataProvider
        todo={{
          _id: "idTestWithNumbers012345",
          label: "Wipro background check",
          done: false
        }}
        toggleCheckboxChange={toggleCheckboxFunction}
        myTodos={false}
      />
    );
    /** mock todo object for mock parameter */
    const mocTodo = {
      _id: "idTestWithNumbers012345",
      label: "Wipro background check",
      done: false
    };
    /** call the function that should NOT call the put todo api because 'myTodos' is false */
    await todoDataProvider2.instance().setTodoDone(mocTodo, false);
    /** expect the function to be called once */
    expect(mockAxios.put).toHaveBeenCalledTimes(0);
    /** expect the action function to also be called */
    expect(toggleCheckboxFunction).toHaveBeenCalledTimes(1);
    /** clear mock axios calls before next test is run */
    jest.clearAllMocks();
  });
});
