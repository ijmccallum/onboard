import React from "react";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import Todo from "./Todo";

describe("<Todo />", () => {
  it("should render a div with a checkbox and label", () => {
    const clickFunction = jest.fn();
    const todoComponent = mount(
      <Todo
        todo={{
          _id: "idTestWithNumbers012345",
          label: "Wipro background check",
          done: false
        }}
        setTodoDone={clickFunction}
      />
    );

    expect(todoComponent.find("input").length).toBe(1);
    expect(todoComponent.find("label").length).toBe(1);
  });

  it("input click should call function", () => {
    const clickFunctionTest = jest.fn();
    const todoComponent = mount(
      <Todo
        todo={{
          _id: "idTestWithNumbers012345",
          label: "Wipro background check",
          done: false
        }}
        setTodoDone={clickFunctionTest}
      />
    );

    todoComponent.find("input").simulate("change");
    expect(clickFunctionTest.mock.calls.length).toBe(1);
  });
});
