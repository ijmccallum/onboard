import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import Todo from "./Todo";

class TodoDataProvider extends Component {
  constructor(props) {
    super(props);

    this.setTodoDone = this.setTodoDone.bind(this);
    this.removeTodo = this.removeTodo.bind(this);
  }

  async setTodoDone(todo, todoDone) {
    try {
      /** only update database for 'My Todos' */
      if (this.props.myTodos) {
        const callUrl = "/api/users/todos/todo/";

        /** set body values for the request */
        const requestBody = {
          userId: this.props.userId,
          todoId: todo._id,
          todoDone: todoDone
        };

        /** rest call api to update database todo */
        await axios.put(callUrl, requestBody);
      }
      this.props.toggleCheckboxChange(todo);
    } catch (error) {
      console.log("ERROR in TodoDataProvider:", error);
    }
  }

  async removeTodo(todo) {
    try {
      if (this.props.myTodos) {
        const callUrl = "/api/users/todos/todo";
        /** set body values for the request */
        const requestBody = {
          data: {
            userId: this.props.userId,
            todoId: todo._id
          }
        };
        /** rest call api to update database todo */
        await axios.delete(callUrl, requestBody);
        /** remove todo from store */
        this.props.deleteTodo(todo);
      }
    } catch (error) {
      console.log("ERROR in TodoDataProvider:", error);
    }
  }

  render() {
    return (
      <Todo
        {...this.props}
        setTodoDone={this.setTodoDone}
        removeTodo={this.removeTodo}
      />
    );
  }
}

TodoDataProvider.propTypes = {
  myTodos: PropTypes.bool.isRequired,
  todo: PropTypes.object.isRequired,
  userId: PropTypes.string,
  toggleCheckboxChange: PropTypes.func.isRequired
};

export default TodoDataProvider;
