import { connect } from "react-redux";
import { SET_TODO_DONE } from "../../../store/todos.js";
import {
  TOGGLE_USERTODO_DONE,
  REMOVE_MYTODO
} from "../../../store/currentUser.js";
import { todoSelector } from "../../../selectors/todoSelector.js";
import TodoDataProvider from "./TodoDataProvider";

const mapStateToProps = (store, props) => ({
  todo: todoSelector(store, props)
});

const mapDispatchToProps = (dispatch, props) => {
  let toggleCheckboxChange;
  let deleteTodo;
  if (props.myTodos) {
    toggleCheckboxChange = todo => dispatch(TOGGLE_USERTODO_DONE(todo));
    deleteTodo = todo => dispatch(REMOVE_MYTODO(todo));
  } else {
    toggleCheckboxChange = todo => dispatch(SET_TODO_DONE(todo));
  }
  return {
    toggleCheckboxChange: toggleCheckboxChange,
    deleteTodo: deleteTodo
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoDataProvider);
