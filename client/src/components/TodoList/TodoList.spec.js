import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });

import TodoList from "./TodoList.js";

describe("<TodoList />", function() {
  it("should render without throwing an error", function() {
    const renderedList = shallow(
      <TodoList
        title="test"
        loading={false}
        error={{}}
        userId={"userIdTest12345"}
        myTodos={true}
      />
    );
    expect(renderedList.find("p").text()).toBe("No todos... yet");
  });

  it("should render [] without throwing an error", function() {
    const renderedList = shallow(
      <TodoList
        title="test"
        todos={[]}
        loading={false}
        error={{}}
        userId={"userIdTest12345"}
        myTodos={true}
      />
    );
    expect(renderedList.find("p").text()).toBe("No todos... yet");
  });

  it("should render a list when there are passed in todos", function() {
    const renderedList = shallow(
      <TodoList
        title="test"
        todos={[{ label: "test todo", done: false }]}
        loading={false}
        error={{}}
        userId={"userIdTest12345"}
        myTodos={true}
      />
    );
    expect(renderedList.find('[data-testid="todolist-li"]').length).toBe(1);
  });
});
