import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import TodoListDataProvider from "./TodoListDataProvider.js";
const propsForDumbComp = {
  title: "test",
  loading: false,
  error: {
    userError: "",
    systemError: ""
  },
  todos: []
};
import mockAxios from "axios";
jest.mock("axios");

describe("<TodoListDataProvider />", function() {
  it("Should get todos if none are cached", async function() {
    const setTodosFn = jest.fn(); //when this is called, set done
    await setTodosFn();
    shallow(
      <TodoListDataProvider
        setTodos={setTodosFn}
        {...propsForDumbComp}
        userId={"userIdTest12345"}
        myTodos={true}
      />
    );
    expect(setTodosFn.mock.calls.length).toBe(1);
  });

  it("Should do nothing if the request is still loading", async function() {
    const setTodosFn = jest.fn();
    shallow(
      <TodoListDataProvider
        setTodos={setTodosFn}
        {...propsForDumbComp}
        loading={true}
        userId={"userIdTest12345"}
        myTodos={true}
      />
    );
    expect(setTodosFn.mock.calls.length).toBe(0);
  });
});
