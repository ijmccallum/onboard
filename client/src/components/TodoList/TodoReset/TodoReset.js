import React from "react";
import Button from "../../elements/Button";
import axios from "axios";

class TodoReset extends React.Component {
  constructor(props) {
    super(props);
    this.refreshTodos = this.refreshTodos.bind(this);
  }

  render() {
    return (
      this.props.myTodos && (
        <Button transparent={false} onClick={() => this.refreshTodos()}>
          Reset Todos
        </Button>
      )
    );
  }

  async refreshTodos() {
    try {
      const response = await axios.post("/api/users/todos/refresh", {
        userId: this.props.userId
      });
      /** parse response to json */
      const responseJson = JSON.parse(response.request.response);
      const newTodos = responseJson.todos;
      /** update store with new todos for current user */
      this.props.refreshTodos(newTodos);
    } catch (error) {
      console.error(
        error,
        "There has been an error refreshing the todos, please contact your administrator."
      );
    }
  }
}

export default TodoReset;
