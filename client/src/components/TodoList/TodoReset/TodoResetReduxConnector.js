import { connect } from "react-redux";
import TodoReset from "./TodoReset";
import { RESET_MYTODOS } from "../../../store/currentUser";

const mapStateToProps = (store, props) => ({});

const mapDispatchToProps = dispatch => ({
  refreshTodos: newTodos => dispatch(RESET_MYTODOS(newTodos))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoReset);
