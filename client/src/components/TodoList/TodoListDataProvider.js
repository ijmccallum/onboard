import React, { Component } from "react";
import PropTypes from "prop-types";
import TodoList from "./TodoList";
import TodoAddReduxConnector from "./TodoAdd/TodoAddReduxConnector";
import TodoResetReduxConnector from "./TodoReset/TodoResetReduxConnector";
import axios from "axios";

/**
 * Fetches data from the api
 */

class TodoListDataProvider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false
    };

    if (props.todos.length == 0) {
      this.getTodosData();
    }
  }

  async getTodosData() {
    try {
      this.setState({
        loading: true
      });
      const response = await axios.get("/api/todos");
      this.props.setTodos(response.data.todos);
      this.setState({
        loading: false
      });
    } catch (error) {
      console.error(
        error,
        "There has been an error retreiving the data for ToDos, please contact your administrator."
      );
    }
  }

  render() {
    if (this.state.loading) {
      return <p>Loading...</p>;
    }
    return (
      <div>
        <TodoAddReduxConnector
          myTodos={this.props.myTodos}
          userId={this.props.userId}
        />
        <TodoList {...this.props} />
        <TodoResetReduxConnector
          myTodos={this.props.myTodos}
          userId={this.props.userId}
        />
      </div>
    );
  }
}

TodoListDataProvider.propTypes = {
  todos: PropTypes.array,
  setTodos: PropTypes.func.isRequired,
  userId: PropTypes.string
};

export default TodoListDataProvider;
