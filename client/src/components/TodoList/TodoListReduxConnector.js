import { connect } from "react-redux";
import TodoListDataProvider from "./TodoListDataProvider.js";
import { SET_TODOS } from "../../store/todos.js";
import { todosSelector } from "../../selectors/todosSelectors.js";
import { userIdSelector } from "../../selectors/currentUserSelector.js";

const mapStateToProps = (store, props) => ({
  todos: todosSelector(store, props),
  userId: userIdSelector(store, props)
});

const mapDispatchToProps = dispatch => ({
  setTodos: todos => dispatch(SET_TODOS(todos))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoListDataProvider);
