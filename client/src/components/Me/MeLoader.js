import React, { Component } from "react";
import Loadable from "react-loadable";

const Loader = Loadable({
  loader: () => import("./MeReduxConnector.js"),
  loading() {
    return <p>loading...</p>;
  }
});

class MeLoader extends Component {
  render() {
    return <Loader />;
  }
}

export default MeLoader;
