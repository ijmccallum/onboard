import React, { Component } from "react";
import PropTypes from "prop-types";
import UserIcon from "../elements/Icons/User";
import SadDino from "../elements/Icons/SadDino";
import Button from "../elements/Button";
import PadawanList from "../PadawanList/PadawanList.js";
import BuddyList from "../BuddyList/BuddyList.js";
import TodoListLoader from "../TodoList/TodoListLoader.js";
import Card from "../blocks/Card";
import Page from "../blocks/Page";

/**
 * the ME page
 */

class Me extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    //TODO: current user detail, get data from currentUser store
    return (
      <Page.Wrapper>
        <Page.Title data-testid="userobject-name">
          {this.props.user.name}, {this.props.user.wiproemail}
        </Page.Title>
        <Card.Wrapper>
          <div className="flex justify-between items-center ph3">
            {this.props.user.wiproemail}
            <Button
              className="button tc"
              onClick={this.props.deleteMe}
              data-testid="userobject-delete-btn"
            >
              <span className="mr2">Delete yourself</span> <SadDino />
            </Button>
          </div>
        </Card.Wrapper>
        <Card.Wrapper className="mv2">
          <Card.Title>Your buddies</Card.Title>
          <BuddyList buddies={this.props.user.buddies} />
        </Card.Wrapper>
        <Card.Wrapper className="mv2">
          <Card.Title>Your todos</Card.Title>
          <TodoListLoader myTodos={true} />
        </Card.Wrapper>
        <Card.Wrapper className="mv2">
          <Card.Title>Your padawans</Card.Title>
          <PadawanList
            padawans={this.props.user.padawans}
            rejectPadawan={this.props.rejectPadawan}
          />
        </Card.Wrapper>
      </Page.Wrapper>
    );
  }
}

Me.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    wiproemail: PropTypes.string.isRequired,
    padawans: PropTypes.array,
    todos: PropTypes.array,
    buddies: PropTypes.array
  }).isRequired,
  deleteMe: PropTypes.func.isRequired
};

export default Me;
