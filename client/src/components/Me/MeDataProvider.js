import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import Me from "./Me.js";
import axios from "axios";

/**
 * Get the user's data
 */

class MeDataProvider extends Component {
  constructor(props) {
    super(props);

    this.deleteMe = this.deleteMe.bind(this);
    this.rejectPadawan = this.rejectPadawan.bind(this);
  }

  async deleteMe() {
    try {
      const removeRes = await axios.delete("/api/users", {
        data: {
          _id: this.props.user._id
        }
      });

      this.props.removeUser(this.props.user);
    } catch (error) {
      console.error("remove user err:R", error);
    }
  }

  async rejectPadawan(padawan) {
    //to make this user your padawan
    try {
      const rejectedPadawanRes = await axios.delete("/api/me/padawans", {
        data: {
          _id: padawan._id
        }
      });

      this.props.removePadawan(padawan); //TODO: this
    } catch (error) {
      console.error("Make padawan error", error);
    }
  }

  render() {
    return (
      <Me
        {...this.props}
        deleteMe={this.deleteMe}
        rejectPadawan={this.rejectPadawan}
      />
    );
  }
}

MeDataProvider.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string
  }),
  deleteMe: PropTypes.func.isRequired,
  removePadawan: PropTypes.func.isRequired
};

export default MeDataProvider;
