import { connect } from "react-redux";
import { REMOVE_USER } from "../../store/users.js";
import { REMOVE_PADAWAN_FROM_ME } from "../../store/currentUser.js";
import MeDataProvider from "./MeDataProvider.js";

const mapStateToProps = store => ({
  user: store.currentUser.userData
});

const mapDispatchToProps = dispatch => ({
  deleteMe: user => dispatch(REMOVE_USER(user)),
  removePadawan: padawan => dispatch(REMOVE_PADAWAN_FROM_ME(padawan))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MeDataProvider);
