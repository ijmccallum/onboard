import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Li } from "./styles";
import PropTypes from "prop-types";

class NavBarLink extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      this.props.render && (
        <Li
          selected={this.props.location.pathname == this.props.to}
          colour={this.props.colour}
        >
          <Link to={this.props.to}>{this.props.text}</Link>
        </Li>
      )
    );
  }
}

NavBarLink.propTypes = {
  to: PropTypes.string,
  text: PropTypes.string,
  colour: PropTypes.string,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired
};

export default NavBarLink;
