import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import NavBarLink from "./NavBarLink.js";

describe("<NavBarLink />", () => {
  it("should render a link component from react router", () => {
    const navBarLinkComponent = shallow(
      <NavBarLink
        to="/test"
        text="test link"
        colour="#ffffff"
        location={{ pathname: "/test" }}
      />
    );

    //TODO: make this test a bit more robust... styled components / react router getting in the way
    expect(navBarLinkComponent.length).toBe(1);
  });
});
