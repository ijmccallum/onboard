import styled from "styled-components";

const Li = styled.li`
  border-bottom: 5px solid;
  border-bottom-color: ${props => props.colour};
  a {
    text-decoration: none;
    text-transform: uppercase;
    ${props => (props.selected ? "pointer-events: none" : "")};
    color: ${props => (props.selected ? props.colour : "#fff")};
    padding: 10px;
    display: inline-block;
  }
  a:hover {
    color: #d3d3d3;
  }
`;

export { Li };
