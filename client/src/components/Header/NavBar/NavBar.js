import React, { Component } from "react";
import NavBarLink from "./NavBarLink/NavBarLink.js";
import { Nav, Ul } from "./styles";

class NavBar extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Nav>
        <Ul>
          <NavBarLink
            to="/"
            text="Home"
            colour="#3fc631"
            render={true}
            {...this.props}
          />
          <NavBarLink
            to="/me"
            text="Me"
            colour="#ff0037"
            render={this.props.isLoggedIn}
            {...this.props}
          />
          <NavBarLink
            to="/boilerplate-todos"
            text="Boilerplate Todos"
            colour="#ffdb00"
            render={this.props.isLoggedIn}
            {...this.props}
          />
          <NavBarLink
            to="/users"
            text="Users"
            colour="#00a3e6"
            render={this.props.isLoggedIn}
            {...this.props}
          />
        </Ul>
      </Nav>
    );
  }
}

export default NavBar;
