import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import NavBar from "./NavBar.js";

describe("<NavBar />", () => {
  it("should render a nav", () => {
    const navBarComponent = shallow(
      <NavBar
        to="/test"
        text="test link"
        colour="#ffffff"
        location={{ pathname: "/test" }}
      />
    );

    //TODO: make this test a bit more robust... styled components / react router getting in the way
    expect(navBarComponent.length).toBe(1);
  });
});
