import styled from "styled-components";

const Nav = styled.nav`
  padding: 10px;
`;

const Ul = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
  display: flex;
`;

export { Ul, Nav };
