import { connect } from "react-redux";
import NavBar from "./NavBar.js";

const mapStateToProps = store => ({
  isLoggedIn: store.currentUser.isLoggedIn
});

export default connect(mapStateToProps)(NavBar);
