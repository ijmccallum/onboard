import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import Header from "./Header.js";

describe("<Header />", () => {
  it("should render a Header", () => {
    const headerComponent = shallow(<Header />);

    //TODO: make this test a bit more robust... although it's not really doing much of anything.
    expect(headerComponent.length).toBe(1);
  });
});
