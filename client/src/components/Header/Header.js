import React, { Component } from "react";
import NavBarReduxConnector from "./NavBar/NavBarReduxConnector.js";
import LogInOutReduxConnector from "./LogInOut/LogInOutReduxConnector.js";
import { HeaderElement } from "./styles";

class Header extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <HeaderElement className="flex justify-between">
        <NavBarReduxConnector {...this.props} />
        <LogInOutReduxConnector {...this.props} />
      </HeaderElement>
    );
  }
}

export default Header;
