import React, { Component } from "react";
import PropTypes from "prop-types";
import LogInOutFormikker from "./LogInOutFormikker.js";
import axios from "axios";
import Button from "../../elements/Button";
import ExitIcon from "../../elements/Icons/Exit.js";

/**
 * Talk to the API because the hand of the king was shot through by an arrow
 */

class LogInOutDataProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      error: false
    };
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
  }

  async login({ wiproemail, password }) {
    try {
      this.setState({ loading: true });
      const response = await axios.post("/api/login", {
        wiproemail,
        password
      });

      this.props.setLoggedIn(response.data);
      this.setState({ loading: false });
    } catch (error) {
      console.error(
        "Header/LogInOut/LogInOutDataProvider.js login api call error: ",
        error
      );
      this.setState({
        error:
          "There has been an error authenticating you, please contact your local laird.",
        loading: false
      });
    }
  }

  async logout() {
    try {
      this.setState({ loading: true });
      const response = await axios.post("/api/logout");
      this.props.setLoggedIn(false);
      this.setState({ loading: false });
    } catch (error) {
      console.error(
        "Header/LogInOut/LogInOutDataProvider.js logout api call error: ",
        error
      );
      this.setState({
        error:
          "There has been an error unauthenticating you, please contact your local laird.",
        loading: false
      });
    }
  }

  render() {
    if (this.props.isLoggedIn) {
      return (
        <Button onClick={this.logout} data-testid="logout-btn">
          <span className="mr2">Log out</span> <ExitIcon />
        </Button>
      );
    }
    return (
      <LogInOutFormikker
        {...this.props}
        login={this.login}
        logout={this.logout}
        loading={this.state.loading}
      />
    );
  }
}

LogInOutDataProvider.propTypes = {
  setLoggedIn: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired
};

export default LogInOutDataProvider;
