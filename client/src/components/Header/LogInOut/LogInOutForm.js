import React from "react";
import PropTypes from "prop-types";
import Input from "../../elements/Input";
import Button from "../../elements/Button";

// Our inner form component which receives our form's state and updater methods as props
const LogInOutForm = ({
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  handleSubmit,
  loading
}) => (
  <form onSubmit={handleSubmit} className="flex">
    <Input.Wrapper>
      <Input.Input
        colour="#ffdb00"
        type="email"
        name="email"
        data-testid="login-email"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.email}
      />
      <Input.Helper>
        {touched.email && errors.email && <div>{errors.email}</div>}
      </Input.Helper>
    </Input.Wrapper>
    <Input.Wrapper>
      <Input.Input
        colour="#ff0037"
        type="password"
        name="password"
        data-testid="login-password"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.password}
      />
      <Input.Helper>
        {touched.password && errors.password && <div>{errors.password}</div>}
      </Input.Helper>
    </Input.Wrapper>
    <Button type="submit" disabled={loading} data-testid="login-submit">
      Submit
    </Button>
  </form>
);

LogInOutForm.propTypes = {
  values: PropTypes.shape({
    email: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired
  }),
  errors: PropTypes.shape({
    email: PropTypes.string,
    password: PropTypes.string
  }),
  touched: PropTypes.shape({
    email: PropTypes.bool,
    password: PropTypes.bool
  }),
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
};

export default LogInOutForm;
