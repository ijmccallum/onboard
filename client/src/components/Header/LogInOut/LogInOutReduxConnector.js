import { connect } from "react-redux";
import LogInOutDataProvider from "./LogInOutDataProvider.js";

import { SET_LOGGEDIN, SET_LOGGEDOUT } from "../../../store/currentUser.js";

const mapStateToProps = store => ({
  isLoggedIn: store.currentUser.isLoggedIn
});

const mapDispatchToProps = dispatch => ({
  setLoggedIn: userData => {
    if (userData.name) {
      dispatch(SET_LOGGEDIN(userData));
    } else {
      dispatch(SET_LOGGEDOUT());
    }
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogInOutDataProvider);
