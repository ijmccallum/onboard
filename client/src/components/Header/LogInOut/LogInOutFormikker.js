import React, { Component } from "react";
import { Formik } from "formik";
import LogInOutForm from "./LogInOutForm.js";

class LogInOutFormikker extends Component {
  constructor(props) {
    super(props);

    this.validateValues = this.validateValues.bind(this);
    this.submissionHandler = this.submissionHandler.bind(this);
  }

  validateValues(values, props) {
    const errors = {};
    if (!values.email) {
      errors.email = "Required";
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
      errors.email = "Invalid email address";
    }
    return errors;
  }

  /* gets setValues, setStatus, and other goodies */
  submissionHandler(formValues) {
    this.props.login({
      wiproemail: formValues.email,
      password: formValues.password
    });
  }

  render() {
    return (
      <Formik
        initialValues={{
          email: "editest@wipro.com",
          password: "test"
        }}
        validate={this.validateValues}
        onSubmit={this.submissionHandler}
        render={props => {
          return <LogInOutForm {...this.props} {...props} />;
        }}
      />
    );
  }
}

export default LogInOutFormikker;
