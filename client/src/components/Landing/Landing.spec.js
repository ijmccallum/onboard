import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
import Landing from "./Landing.js";

describe("<Landing />", () => {
  it("should render a bunch of p tags with words in them", () => {
    const landingComponent = shallow(<Landing />);

    expect(landingComponent.find("p").length).toBeGreaterThan(1);
  });
});
