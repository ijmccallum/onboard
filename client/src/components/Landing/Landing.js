import React, { Component } from "react";
import { HomeWrapper } from "./styles";

class Landing extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <HomeWrapper>
        <div>
          <h2>{t("landing.welcome")}</h2>
          <h2>{t("landing.welcome.nope")}</h2>
          <p>
            The basics of our Edinburgh studio, who works here, and how we use
            the space to come together and the things you need to know when
            working in the Studio. The space will support all we do, whether
            brainstorming and gathering ideas, defining, designing, or building.
            It is a place to support our ways of working, adapt to our evolving
            needs and allow people and information to flow. We hope you find it
            a people focussed space that is welcoming to us all, our colleagues
            and clients.
          </p>
        </div>
        <div>
          <h3>Who is based at the Studio?</h3>
          <p>
            The Studio brings Wipro Digital (Buildit) and Designit teams in
            Edinburgh together in one place. A single space for activity based
            working, lab away days, group discussions and meetings.
          </p>
          <p>
            Sonata Sadauskaite is our office manager and David Nicholas is our
            IT systems engineer, they are both permanently based in the studio.
          </p>
          <p>
            The Studio provides a mix of shared, team and project spaces. The
            spaces are there to support the type of activity individuals do.
            Whatever you need to do there is a space that should work for you.
          </p>
          <p>
            No one has a dedicated seat in The Studio. Find a seat in an area
            that works for you and be respectful of how other are using the
            space.
          </p>
          <p>
            There are more people based or with access to The Studio than there
            are desks. People spend time away from base on client work and so
            day-to-day it shouldn’t be a problem. There are loads of ‘touchdown’
            areas, and seating in the shared areas for days it is a bit hectic.
          </p>
        </div>
      </HomeWrapper>
    );
  }
}

export default Landing;
