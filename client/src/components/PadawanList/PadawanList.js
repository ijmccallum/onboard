import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import PunchIcon from "../elements/Icons/Punch.js";
import { ListItem } from "../blocks/ListItem";
import Button from "../elements/Button";
import SadDino from "../elements/Icons/SadDino.js";
import StudentIcon from "../elements/Icons/Student.js";

/**
 * Preview of a user
 */

class PadawanList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const rejectPadawan = this.props.rejectPadawan;
    return (
      <ul className="list pa0 ma0">
        {this.props.padawans.map(padawan => {
          return (
            <ListItem key={padawan._id} data-testid="padawan-list-item">
              <Link
                to={`/users/${padawan._id}`}
                data-testid="padawan-list-item-link"
                className="no-underline flex items-center justify-between pa2 f6"
              >
                <StudentIcon />

                <div data-testid="user-list-item-name" className="mh2">
                  {padawan.name}
                </div>
              </Link>
              <Button
                className="button tc"
                onClick={() => {
                  rejectPadawan(padawan);
                }}
                data-testid="padawan-rejection-btn"
              >
                <span className="mr2">Reject padawan</span> <PunchIcon />
              </Button>
            </ListItem>
          );
        })}
      </ul>
    );
  }
}

PadawanList.propTypes = {
  padawans: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      wiproemail: PropTypes.string.isRequired,
      personalemail: PropTypes.string,
      _id: PropTypes.string.isRequired
    })
  ),
  rejectPadawan: PropTypes.func.isRequired
};

export default PadawanList;
