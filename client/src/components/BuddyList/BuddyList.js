import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import PunchIcon from "../elements/Icons/Punch.js";
import { ListItem } from "../blocks/ListItem";
import Button from "../elements/Button";
import SadDino from "../elements/Icons/SadDino.js";
import BuddyIcon from "../elements/Icons/Buddy.js";

/**
 * Preview of a buddy
 */

class BuddyList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ul className="list pa0 ma0">
        {this.props.buddies.map(buddy => {
          return (
            <ListItem key={buddy._id} data-testid="buddy-list-item">
              <Link
                to={`/users/${buddy._id}`}
                data-testid="buddy-list-item-link"
                className="no-underline flex items-center justify-between pa2 f6"
              >
                <BuddyIcon />

                <div data-testid="user-list-item-name" className="mh2">
                  {buddy.name}
                </div>
              </Link>
              <input type="text" value="" placeholder="Make a Cry for Help" />
            </ListItem>
          );
        })}
      </ul>
    );
  }
}

BuddyList.propTypes = {
  buddies: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      wiproemail: PropTypes.string.isRequired,
      personalemail: PropTypes.string,
      _id: PropTypes.string.isRequired
    })
  )
};

export default BuddyList;
