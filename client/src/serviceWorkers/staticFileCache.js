/**
 * Caching the static files that make up the ap itself
 */
var CACHE_NAME = "static";
var urlsToCache = ["/", "/styles/main.css", "/script/main.js"];

export default () => {
  //no caching on install unfortunatly - files are hashed, woo!
  //unless there's a way to tell the sw of the hases on install.

  self.addEventListener("fetch", function(event) {
    // console.log("event", event);
    //if event request ends with .js, cache it!
    event.respondWith(
      caches.open(CACHE_NAME).then(function(cache) {
        return fetch(event.request)
          .then(function(response) {
            //if the request url contains "api" don't cache it... ?
            //if the network responds, cache it
            try {
              //TODO: catch POST requests while offline and queue them for later posting when back online.
              cache.put(event.request, response.clone());
            } catch (err) {
              console.log("failed to cache request: ", err);
            }
            return response;
          })
          .catch(function() {
            //if nothing comes back from the network, serve the cache
            return caches.match(event.request);
          });
      })
    );
  });
};
