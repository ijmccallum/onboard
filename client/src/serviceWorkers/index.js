import staticFileCache from "./staticFileCache.js";
import apiTodoCache from "./apiTodoCache.js";

export default () => {
  staticFileCache();
  // apiTodoCache();
};
