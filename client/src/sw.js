/**
 * Service workers are compiled into their own file.
 * Installed by a wee script in the index.html file
 * The url of the sw.js file determins the sw's scope
 * https://developers.google.com/web/fundamentals/primers/service-workers/
 */
import serviceWorker from "./serviceWorkers";
serviceWorker();
