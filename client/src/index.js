import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import AppReduxConnector from "./appReduxConnector.js";
import "./styles/globals.scss";
import store from "./store";
import initI18n from "./i18n";

let appDiv = document.getElementById("app");

const run = async function() {
  await initI18n();

  ReactDOM.render(
    <Provider store={store}>
      <AppReduxConnector />
    </Provider>,
    appDiv
  );
};

run();
