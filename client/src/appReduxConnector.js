import { connect } from "react-redux";
import App from "./app.js";
import { SET_LOGGEDIN } from "./store/currentUser.js";
import { RENDER_APP } from "./store/app.js";

const mapStateToProps = store => ({
  userData: store.currentUser.userData,
  isLoggedIn: store.currentUser.isLoggedIn,
  render: store.app.render
});

const mapDispatchToProps = dispatch => ({
  setUserData: userData => {
    if (userData.name) {
      dispatch(SET_LOGGEDIN(userData));
    } else {
      dispatch(SET_LOGGEDIN(false));
    }
  },
  renderApp: app => {
    dispatch(RENDER_APP(app));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
