import currentUserReducer, {
  SET_LOGGEDIN,
  SET_LOGGEDOUT,
  ADD_PADAWAN_TO_ME
} from "./currentUser.js";

describe("currebntUser reducer", () => {
  //TODO: this
  // it("should handle initial state", () => {
  //   expect(currentUserReducer()).toEqual({
  //     isLoggedIn: false,
  //     userData: false
  //   });
  // });

  it("should handle SET_LOGGEDIN with user data", () => {
    expect(currentUserReducer({}, SET_LOGGEDIN({ hi: "hi" }))).toEqual({
      userData: { hi: "hi" },
      isLoggedIn: true
    });
  });

  it("should handle SET_LOGGEDIN without user data", () => {
    expect(currentUserReducer({}, SET_LOGGEDOUT())).toEqual({
      userData: {},
      isLoggedIn: false
    });
  });

  it("should handle ADD_PADAWAN_TO_ME", () => {
    expect(
      currentUserReducer(
        {
          userData: {
            padawans: []
          }
        },
        ADD_PADAWAN_TO_ME({
          hi: "hi"
        })
      )
    ).toEqual({
      userData: {
        padawans: [
          {
            hi: "hi"
          }
        ]
      }
    });
  });
});
