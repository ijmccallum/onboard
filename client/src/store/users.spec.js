import usersReducer, { SET_USERS, ADD_USER, REMOVE_USER } from "./users.js";

// test variables
const initialStateTestObject = {
  users: [{ hi: "hi1" }]
};

describe("users reducer", () => {
  it("should handle initial state", () => {
    expect(usersReducer(initialStateTestObject, [])).toEqual(
      initialStateTestObject
    );
  });

  it("should handle SET_USERS", () => {
    expect(
      usersReducer(initialStateTestObject, SET_USERS([{ hi: "hi" }]))
    ).toEqual({
      users: [{ hi: "hi" }]
    });
  });

  it("should handle ADD_USER", () => {
    expect(
      usersReducer(initialStateTestObject, ADD_USER([{ hi: "hi2" }])).users
        .length
    ).toEqual(2);
  });

  it("should handle REMOVE_USER", () => {
    expect(
      usersReducer(
        { users: [{ wiproemail: "hi1" }] },
        REMOVE_USER({ wiproemail: "hi1" })
      ).users.length
    ).toEqual(0);
  });
});
