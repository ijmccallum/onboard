import { createAction, handleActions } from "redux-actions";

/**
 * Actions, reducers, for all the todocers
 */

export const SET_TODOS = createAction("SET_TODOS");
export const SET_TODO_DONE = createAction("SET_TODO_DONE");

const initialState = {
  todos: []
};

const setTodosReducer = (state, action) => {
  return {
    ...state,
    todos: action.payload
  };
};

const setTodoToggleReducer = (state, action) => {
  return {
    ...state,
    todos: state.todos.map(todo => {
      if (todo.label === action.payload.label) {
        return {
          ...todo,
          done: !todo.done
        };
      }
      return { ...todo };
    })
  };
};

const todosReducer = handleActions(
  {
    [SET_TODOS]: setTodosReducer,
    [SET_TODO_DONE]: setTodoToggleReducer
  },
  initialState
);

export default todosReducer;
