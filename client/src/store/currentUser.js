import { handleActions, createAction } from "redux-actions";
import { create } from "domain";

export const SET_LOGGEDIN = createAction("SET_LOGGEDIN");
export const SET_LOGGEDOUT = createAction("SET_LOGGEDOUT");
export const TOGGLE_USERTODO_DONE = createAction("SET_USERTODO_DONE");
export const ADD_PADAWAN_TO_ME = createAction("ADD_PADAWAN_TO_ME");
export const REMOVE_PADAWAN_FROM_ME = createAction("REMOVE_PADAWAN_FROM_ME");
export const ADD_MYTODO = createAction("ADD_MYTODO");
export const REMOVE_MYTODO = createAction("REMOVE_MYTODO");
export const RESET_MYTODOS = createAction("RESET_MYTODOS");

const initialState = {
  userData: {
    _id: "",
    name: "",
    wiproemail: "",
    personalemail: "",
    padawans: [],
    buddies: [],
    todos: []
  },
  isLoggedIn: false
};

const currentUserReducer = handleActions(
  {
    [SET_LOGGEDIN]: (state, action) => {
      return {
        userData: action.payload,
        isLoggedIn: true
      };
    },
    [SET_LOGGEDOUT]: (state, action) => {
      return {
        userData: {},
        isLoggedIn: false
      };
    },
    [TOGGLE_USERTODO_DONE]: (state, action) => {
      return {
        ...state,
        userData: {
          ...state.userData,
          todos: state.userData.todos.map(todo => {
            if (todo._id === action.payload._id) {
              return {
                ...todo,
                done: !todo.done
              };
            }
            return { ...todo };
          })
        }
      };
    },
    [ADD_PADAWAN_TO_ME]: (state, action) => {
      return {
        ...state,
        userData: {
          ...state.userData,
          padawans: [...state.userData.padawans, action.payload]
        }
      };
    },
    [REMOVE_PADAWAN_FROM_ME]: (state, action) => {
      let newPadawans = state.userData.padawans.filter(padawan => {
        return padawan._id !== action.payload._id;
      });
      return {
        ...state,
        userData: {
          ...state.userData,
          padawans: newPadawans
        }
      };
    },
    [ADD_MYTODO]: (state, action) => {
      return {
        ...state,
        userData: {
          ...state.userData,
          todos: [...state.userData.todos, action.payload]
        }
      };
    },
    [REMOVE_MYTODO]: (state, action) => {
      let newTodos = state.userData.todos.filter(
        todo => todo._id !== action.payload._id
      );
      return {
        ...state,
        userData: {
          ...state.userData,
          todos: newTodos
        }
      };
    },
    [RESET_MYTODOS]: (state, action) => {
      const refreshedTodos = action.payload;
      return {
        ...state,
        userData: {
          ...state.userData,
          todos: refreshedTodos
        }
      };
    }
  },
  initialState
);

export default currentUserReducer;
