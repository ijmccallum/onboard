import { createAction, handleActions } from "redux-actions";

/**
 * Actions, reducers, for all the userdusers
 */

export const SET_USERS = createAction("SET_USERS");
export const ADD_USER = createAction("ADD_USER");
export const REMOVE_USER = createAction("REMOVE_USER");

const initialState = {
  users: []
};

const usersReducer = handleActions(
  {
    [SET_USERS]: (state, action) => {
      return {
        ...state,
        users: action.payload
      };
    },
    [ADD_USER]: (state, action) => {
      return {
        ...state,
        users: [...state.users, action.payload.user]
      };
    },
    [REMOVE_USER]: (state, action) => {
      let culledUserArray = state.users.filter(user => {
        return user.wiproemail !== action.payload.wiproemail;
      });
      return {
        ...state,
        users: culledUserArray
      };
    }
  },
  initialState
);

export default usersReducer;
