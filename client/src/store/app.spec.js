import appReducer, { RENDER_APP } from "./app.js";

const initialAppStateTest = {
  render: false
};

describe("app reducer", () => {
  it("should load and handle initial state", () => {
    expect(appReducer(initialAppStateTest, [])).toEqual(initialAppStateTest);
  });
  it("should handle RENDER_APP action", () => {
    expect(appReducer(initialAppStateTest, RENDER_APP(true))).toEqual({
      render: true
    });
  });
});
