import { createStore, combineReducers } from "redux";
import todos from "./todos.js";
import users from "./users.js";
import currentUser from "./currentUser.js";
import app from "./app.js";

const entities = combineReducers({
  app,
  todos,
  currentUser,
  users
});

const store = createStore(
  entities /* preloadedState, */,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
