import { handleActions, createAction } from "redux-actions";

export const RENDER_APP = createAction("RENDER_APP");

const initialAppState = {
  // setting to false initially, reduces flicker on page refresh
  render: false
};

const appReducer = handleActions(
  {
    [RENDER_APP]: (state, action) => {
      return {
        ...state,
        render: action.payload
      };
    }
  },
  initialAppState
);

export default appReducer;
