import todosReducer, { SET_TODOS, SET_TODO_DONE } from "./todos.js";

// test variables
const initialStateTestObject = {
  todos: []
};

describe("todos reducer", () => {
  it("should handle initial state", () => {
    expect(todosReducer(initialStateTestObject, [])).toEqual(
      initialStateTestObject
    );
  });

  it("should handle SET_TODOS", () => {
    expect(
      todosReducer(initialStateTestObject, SET_TODOS([{ hi: "hi" }]))
    ).toEqual({
      todos: [{ hi: "hi" }]
    });
  });

  it("should handle SET_TODO_DONE", () => {
    expect(
      todosReducer(
        {
          ...initialStateTestObject,
          todos: [{ label: "test", done: false }, { label: "no", done: false }]
        },
        SET_TODO_DONE({ label: "test", done: false })
      )
    ).toEqual({
      todos: [{ label: "test", done: true }, { label: "no", done: false }]
    });
  });
});
