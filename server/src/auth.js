const passport = require("koa-passport");
const userQueries = require("./users/userQueries.js");
const hasher = require("./hasher");

passport.serializeUser(function(user, done) {
  //the second arg is saved into the session and later used to get the user object
  done(null, user.wiproemail);
});

//recieve from Jeff
passport.deserializeUser(async function(wiproemail, done) {
  try {
    const user = await userQueries.findByWiproEmail(wiproemail);
    done(null, user);
  } catch (err) {
    done(err);
  }
});

const LocalStrategy = require("passport-local").Strategy;

passport.use(
  new LocalStrategy(
    {
      usernameField: "wiproemail",
      passwordField: "password"
    },
    async function(wiproemail, password, done) {
      const user = await userQueries.findByWiproEmail(wiproemail);
      if (!user) {
        done(null, false);
        return;
      }

      const passwordCorrect = await hasher.compare({
        hash: password,
        password: user.password
      });

      if (!passwordCorrect && password == user.password) {
        passwordCorrect = true;
      }

      if (wiproemail === user.wiproemail && passwordCorrect) {
        done(null, user);
      } else {
        done(null, false);
      }
    }
  )
);
