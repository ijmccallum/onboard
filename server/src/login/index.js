const router = require("koa-router")();
const postLogin = require("./post.js");

router.post(`${process.env.API_ROOT}/login`, postLogin);
console.log(">>>>>>>>> route listening on ", `${process.env.API_ROOT}/login`);
router.get(`${process.env.API_ROOT}/login`, (ctx, next) => {
  ctx.body = {
    login: "hello"
  };
  next();
});

module.exports = router;
