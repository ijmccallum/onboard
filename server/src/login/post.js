const passport = require("koa-passport");
const userQuerires = require("../users/userQueries.js");

const postLogin = async (ctx, next) => {
  console.log(">>>>>>>>> login route");
  const user = await userQuerires.findByWiproEmail(ctx.request.body.wiproemail);

  return passport.authenticate("local", (err, user, info, status) => {
    if (user) {
      ctx.login(user);
      ctx.body = {
        _id: user._id,
        name: user.name,
        wiproemail: user.wiproemail,
        personalemail: user.personalemail,
        buddies: user.buddies,
        padawans: user.padawans,
        todos: user.todos
      };
    } else {
      ctx.body = { status: "error" };
    }
  })(ctx);
};

module.exports = postLogin;
