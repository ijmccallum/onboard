const router = require("koa-router")();
const get = require("./get.js");
const putPadawan = require("./padawans/put.js");
const deletePadawan = require("./padawans/delete.js");
// require("./userSchema.js"); //runs on require?

router
  .get(`${process.env.API_ROOT}/me`, get)
  .put(`${process.env.API_ROOT}/me/padawans`, putPadawan)
  .delete(`${process.env.API_ROOT}/me/padawans`, deletePadawan);

module.exports = router;
