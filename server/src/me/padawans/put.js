const userQueries = require("../../users/userQueries.js");

const putPadawan = async (ctx, next) => {
  if (!ctx.isAuthenticated()) {
    ctx.status = 401;
  }
  if (ctx.isAuthenticated()) {
    if (!ctx.request.body.data._id) {
      ctx.body = {
        error: "a user id is required to make a user your padawan"
      };
      ctx.status = 400;
    }

    //get the padawan, should exist
    let padawan = false;
    try {
      padawan = await userQueries.findById(ctx.request.body.data._id);
    } catch (err) {
      ctx.body = {
        error: "Could not find the user to make your padawan."
      };
      ctx.status = 400;
    }

    //add the padawan's id to the current user's padawan array & save to the db
    let currentUser = await userQueries.findById(ctx.state.user._id);
    currentUser.padawans.push({ _id: padawan });
    await currentUser.save();

    //add the current user'r id to the padawan's buddy array & save to the db
    padawan.buddies.push({ _id: currentUser });
    await padawan.save();

    //return the padawan... ? TODO: return db results & make it a bit more defensive
    if (!ctx.body) {
      ctx.body = {};
    }
    ctx.body.newPadawan = padawan;
  }
  next();
};
module.exports = putPadawan;
