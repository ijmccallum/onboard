const userQueries = require("../../users/userQueries.js");

const deletePadawan = async (ctx, next) => {
  if (!ctx.isAuthenticated()) {
    ctx.status = 401;
  }
  if (ctx.isAuthenticated()) {
    if (!ctx.request.body._id) {
      ctx.body = {
        error: "a user id is required to reject your padawan"
      };
      ctx.status = 400;
    }

    //add object id to the current user object & save to the db
    let currentUser = await userQueries.findById(ctx.state.user._id);
    console.log("currentUser before padawan rejection", currentUser);
    currentUser.padawans = currentUser.padawans.filter(padawan => {
      console.log("----- checking", padawan._id !== ctx.request.body._id);
      return padawan._id != ctx.request.body._id;
    });
    await currentUser.save();
    console.log("----- current user with PADAWAN REJECTED!", currentUser);

    //get the current user from the db and return it

    if (!ctx.body) {
      ctx.body = {};
    }
    ctx.body.currentUser = currentUser;
  }
  next();
};
module.exports = deletePadawan;
