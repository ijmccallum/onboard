const router = require("koa-router")();
const Koa = require("koa");
const cors = require("@koa/cors");
const app = new Koa();
const dbSeed = require("./db/seed.js");
app.proxy = true;

var mongoose = require("mongoose");
let connectionAttemptCounter = 0;
function connect() {
  connectionAttemptCounter++;
  if (connectionAttemptCounter > 10) {
    console.error("Failed to connect to mongo");
  } else {
    mongoose.connect(process.env.MONGO_URL);
  }
}

//sessions
const session = require("koa-session");
app.keys = [process.env.SESSION_SECERET];
app.use(session({}, app));

const bodyParser = require("koa-bodyparser");
app.use(bodyParser());

// authentication
require("./auth");
const passport = require("koa-passport");
app.use(passport.initialize());
app.use(passport.session());

if (process.env.NODE_ENV !== "test") {
  mongoose.connection
    .on("error", err => {
      console.log("error", err);
      setTimeout(function() {
        connect();
      }, connectionAttemptCounter * 1000);
    })
    .on("disconnected", () => {
      console.log("disconnected");
    })
    .once("open", () => {
      console.log("connected!!");
      dbSeed();
    });

  connect();
}

app.use(cors());

const staticRouter = require("./static");
const todoRouter = require("./todos");
const usersRouter = require("./users");
const meRouter = require("./me");
const loginRouter = require("./login");
const logoutRouter = require("./logout");
const todosTodo = require("./users/todos");
const localsRouter = require("./locales");

const apiRootRouter = router.get("/api", (ctx, next) => {
  console.log("APIAPIAPIAPIAPAIPIA root");
  ctx.body = {
    hi: "hello"
  };
  next();
});

app
  .use(apiRootRouter.routes())
  .use(todoRouter.routes())
  .use(usersRouter.routes())
  .use(meRouter.routes())
  .use(loginRouter.routes())
  .use(logoutRouter.routes())
  .use(todosTodo.routes())
  .use(localsRouter.routes())
  .use(router.allowedMethods())
  .use(staticRouter.routes()); //static router last - hopefully that works to not block the other routes

module.exports = app;
