const router = require("koa-router")();
const get = require("./get.js");

router.get(`${process.env.API_ROOT}/locales/:lng`, get);

module.exports = router;
