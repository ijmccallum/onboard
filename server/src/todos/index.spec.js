const assert = require("assert");
const todoRouter = require("./index.js");

describe("/todos ", function() {
  it("returns a router listening to /todos", () => {
    assert.equal(todoRouter.stack[0].path, "/todos");
  });
  it("the /todos router has a GET method", () => {
    assert.ok(todoRouter.stack[0].methods.includes("GET"));
  });
});
