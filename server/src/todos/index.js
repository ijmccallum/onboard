const router = require("koa-router")();
const getTodos = require("./get.js");
require("./todoSchema.js"); //runs on require?

router.get(`${process.env.API_ROOT}/todos`, getTodos);

module.exports = router;
