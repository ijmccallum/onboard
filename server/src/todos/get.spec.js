const assert = require("assert");
const getTodos = require("./get.js");

describe("GET todos ", function() {
  it("If authenticated, adds an array of todos to the return object", () => {
    let ctx = {
      body: {},
      isAuthenticated: () => {
        return true;
      }
    };
    getTodos(ctx, () => {
      assert.ok(ctx.body.todos.length);
    });
  });

  it("If not authenticated, returns 401", () => {
    let ctx = {
      body: {},
      isAuthenticated: () => {
        return false;
      }
    };
    getTodos(ctx, () => {
      assert.equal(ctx.status, 401);
      assert.ok(!ctx.body.todos);
    });
  });
});
