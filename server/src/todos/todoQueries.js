const TodoModel = require("./todoSchema.js");

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

const findByLabel = label => {
  return new Promise((resolve, reject) => {
    TodoModel.findOne({ label: label }, (err, todo) => {
      if (err) {
        reject(err);
      }
      resolve(todo);
    });
  });
};

const findById = id => {
  return new Promise((resolve, reject) => {
    TodoModel.findOne({ id: id }, (err, todo) => {
      if (err) {
        reject(err);
      }
      resolve(todo);
    });
  });
};

const findAll = () => {
  return new Promise((resolve, reject) => {
    TodoModel.find((err, todos) => {
      if (err) {
        reject(err);
      }
      resolve(todos);
    });
  });
};

const create = todoObj => {
  return new Promise(async (resolve, reject) => {
    if (!todoObj.label) {
      reject("new todo quires a label");
    }
    let existingTodo = await findByLabel(todoObj.label);
    if (existingTodo) {
      reject("todo already exists");
    }
    TodoModel.create(todoObj, (err, todo) => {
      if (err) {
        reject(err);
      }
      resolve(todo);
    });
  });
};

const createMultiple = todosArray => {
  return new Promise(async (resolve, reject) => {
    let insertArray = [];

    todosArray.forEach(todo => {
      if (!todo.label) {
        reject("every new todo requires a label");
      }
    });

    //weed out any existing todos
    await asyncForEach(todosArray, async todo => {
      let existingTodo = await findByLabel(todo.label);
      if (!existingTodo) {
        insertArray.push(todo);
      }
    });

    TodoModel.collection.insert(insertArray, (err, todos) => {
      if (err) {
        reject(err);
      }
      resolve(todos);
    });
  });
};

module.exports = {
  findByLabel,
  findById,
  findAll,
  create,
  createMultiple
};
