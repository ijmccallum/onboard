var mongoose = require("mongoose");

var TodoSchema = new mongoose.Schema({
  label: String,
  done: Boolean
});

var Todo = mongoose.model("Todo", TodoSchema);

module.exports = Todo;
