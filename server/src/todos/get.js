//https://digitalrig.atlassian.net/wiki/spaces/ENG/pages/127598605
const todoQueries = require("./todoQueries.js");

const getTodos = async (ctx, next) => {
  if (!ctx.isAuthenticated()) {
    ctx.status = 401;
  }
  if (ctx.isAuthenticated()) {
    let todos = await todoQueries.findAll();
    ctx.body = {
      todos: todos
    };
  }
  next();
};
module.exports = getTodos;
