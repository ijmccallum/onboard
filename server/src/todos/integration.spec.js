const request = require("supertest");
const assert = require("assert");
const app = require("../server.js");

describe("GET /todos integration", function() {
  //TODO: this integration test - bit tricky now that there's auth
  // it("if authenticated, returns a list of todos", function(done) {
  //   request(app)
  //     .get("/todos")
  //     .set("Accept", "application/json")
  //     .expect("Content-Type", /json/)
  //     .expect(200)
  //     .end(function(err, res) {
  //       if (err) return done(err);
  //       assert.ok(res.body.todos, "todos to be an array with a length");

  //       done();
  //     });
  // });

  it("if not authenticated, returns a 401", function(done) {
    request(app)
      .get("/todos")
      .set("Accept", "application/json")
      .expect(401)
      .end(function(err, res) {
        done();
      });
  });

  after(function() {
    // runs after all tests in this block
    require("../../src/server.js").stop();
  });
});
