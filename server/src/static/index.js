const router = require("koa-router")();
const path = require("path");
var send = require("koa-send");

router.get("/*", async (ctx, next) => {
  if (ctx.request.method != "GET" && ctx.request.method != "HEAD") {
    return;
  }
  if (ctx.body != null || ctx.status != 404) {
    return;
  }

  var file = ctx.params["0"] || "/" + "index.html";
  var requested = path.normalize(file);
  if (requested.length == 0 || requested == "/" || requested == "/index.html") {
    await send(ctx, "index.html", { root: __dirname + "/dist" });
  } else {
    await send(ctx, ctx.path, { root: __dirname + "/dist" });
  }
});

module.exports = router;
