module.exports = [
  {
    label: "Open this most glorious todo app",
    done: true
  },
  {
    label: "Wipro background check - update twice",
    done: false
  },
  {
    label: "Buddy(s) meet & greet",
    resources: [
      {
        url: "https://digitalrig.atlassian.net/wiki/spaces/ENG/pages/127598605",
        title: "Confluence link"
      }
    ],
    done: false
  },
  {
    label: "Set up MyWipro account",
    done: false
  },
  {
    label: "Get & set up laptop",
    done: false
  },
  {
    label: "Sign into wipro email",
    done: false
  },
  {
    label: "Sign into MyWipro",
    done: false
  },
  {
    label: "Asset allocation (confirm on MyWipro that you have the laptop",
    done: false
  },
  {
    label: "Check / correct personal details on MyWipro",
    done: false
  },
  {
    label: "Set bank details in MyWipro",
    done: false
  },
  {
    label: "Checl / correct line manager in MyWipro",
    done: false
  },
  {
    label: "Set up / sign into Slack",
    done: false
  },
  {
    label: "Set up / sign into Harvest",
    done: false
  },
  {
    label: "Set up / sign into confluence",
    done: false
  },
  {
    label: "Online training: GDPR/Data protection",
    done: false
  },
  {
    label: "Online training: Health & safty",
    done: false
  },
  {
    label: "Online training: fire safty",
    done: false
  },
  {
    label: "Wipro onboarding presentation by HR/Talent Ops",
    done: false
  },
  {
    label: "Wipro onboarding / intro chat - who we are, how we fit into",
    done: false
  }
];
