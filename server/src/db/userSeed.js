const userQueries = require("../users/userQueries.js");
const hasher = require("../hasher");
const todoQueries = require("../todos/todoQueries.js");

/**
 * on startup check if there's data in the db,
 * if not, put some basics in there
 */

module.exports = async () => {
  let testUser = await userQueries.findByWiproEmail("editest@wipro.com");
  if (testUser) {
    return;
  }

  const seedTodos = await todoQueries.findAll();

  testUser = await userQueries.create({
    name: "Mr test",
    wiproemail: "editest@wipro.com",
    personalemail: "test@test.com",
    password: await hasher.hash("test"),
    todos: seedTodos
  });
};
