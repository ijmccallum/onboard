const userSeed = require("./userSeed.js");
const todoSeed = require("./todoSeed.js");

module.exports = () => {
  userSeed();
  todoSeed();
};
