const todoQueries = require("../todos/todoQueries.js");
const todoSeeds = require("./todos.js");
/**
 * on startup check if there's data in the db,
 * if not, put some basics in there
 */

module.exports = async () => {
  try {
    await todoQueries.createMultiple(todoSeeds);
  } catch (err) {
    console.log("---------- create todo seeds err, todos may already exist");
  }
};
