const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  name: String,
  password: String,
  wiproemail: String,
  personalemail: String,
  created: { type: Date, default: Date.now },
  buddies: [
    {
      _id: { type: mongoose.Schema.ObjectId },
      since: { type: Date, default: Date.now }
    }
  ],
  padawans: [
    {
      _id: { type: mongoose.Schema.ObjectId },
      since: { type: Date, default: Date.now }
    }
  ],
  todos: [
    //in user to be customised by/for each user. Built from boilerplate todos when user signs up
    {
      label: String,
      done: Boolean
    }
  ]
});

const User = mongoose.model("User", UserSchema);

module.exports = User;
