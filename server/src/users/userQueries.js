const UserModel = require("./userSchema.js");

const findByWiproEmail = wiproemail => {
  return new Promise((resolve, reject) => {
    UserModel.findOne({ wiproemail }, (err, user) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(user);
    });
  });
};

const findById = _id => {
  return new Promise((resolve, reject) => {
    UserModel.findOne({ _id: _id })
      .populate("padawans")
      .exec((err, user) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(user);
      });
  });
};

const findUserById = userId => {
  return new Promise((resolve, reject) => {
    UserModel.findOne({ _id: userId }, (error, user) => {
      if (error) {
        reject(error);
        return;
      }
      resolve(user);
    });
  });
};

const findAll = () => {
  return new Promise((resolve, reject) => {
    UserModel.find((err, users) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(users);
    });
  });
};

const create = userObj => {
  return new Promise(async (resolve, reject) => {
    if (!userObj.wiproemail) {
      reject("new user re quires a wipro email");
      return;
    }
    let existingUser = await findByWiproEmail(userObj.wiproemail);
    if (existingUser) {
      reject("user already exists");
      return;
    }
    UserModel.create(userObj, (err, user) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(user);
    });
  });
};

const remove = _id => {
  return new Promise(async (resolve, reject) => {
    if (!_id) {
      reject("_id is required to delete a user");
      return;
    }
    let existingUser = await findById(_id);
    if (!existingUser) {
      resolve("could not find user by that id");
      return;
    }

    //remove the user from all padawan lists
    try {
      const padawanRemovalRes = await UserModel.updateMany(
        {
          padawans: {
            $elemMatch: {
              _id: _id
            }
          }
        },
        {
          $pull: {
            padawans: { _id: _id }
          }
        }
      );
    } catch (err) {
      console.log("---------- DB remove user from padawan arrays", err);
    }

    //remove user from all buddy lists
    try {
      const buddyRemovalRes = await UserModel.updateMany(
        {
          buddies: {
            $elemMatch: {
              _id: _id
            }
          }
        },
        {
          $pull: {
            buddies: { _id: _id }
          }
        }
      );
    } catch (err) {
      console.log("---------- DB remove user from bussie arrays", err);
    }

    UserModel.remove({ _id }, (err, res) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

module.exports = {
  findByWiproEmail,
  findUserById,
  findById,
  findAll,
  create,
  remove
};
