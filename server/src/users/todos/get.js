const userTodoQueries = require("./userTodoQueries.js");
//const userQueries = require("../../users/userQueries.js");

const getTodo = async (ctx, next) => {
  if (!ctx.isAuthenticated()) {
    ctx.status = 401;
  }
  if (ctx.isAuthenticated()) {
    // const user = await userQueries.findByWiproEmail(ctx.params.userEmail);
    // const todo = user.todos.filter(todo => todo.id === ctx.params.todoId)[0];
    // ctx.body = todo;
    const todo = await userTodoQueries.getUserTodoById(
      ctx.params.userEmail,
      ctx.params.todoId
    );
    ctx.body = todo;
  }
  next();
};

module.exports = getTodo;
