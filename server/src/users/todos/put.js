const userTodoQueries = require("./userTodoQueries.js");

const putTodoDone = async (ctx, next) => {
  if (!ctx.isAuthenticated()) {
    ctx.status = 401;
  }
  if (ctx.isAuthenticated()) {
    const response = await userTodoQueries.updateUserTodo(
      ctx.request.body.userId,
      ctx.request.body.todoId,
      ctx.request.body.todoDone
    );
    ctx.body = response;
  }
  next();
};

module.exports = putTodoDone;
