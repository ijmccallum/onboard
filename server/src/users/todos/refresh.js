const todoQueries = require("../../todos/todoQueries.js");
const userQueries = require("../userQueries.js");

const refreshMyTodos = async (ctx, next) => {
  if (!ctx.isAuthenticated()) {
    ctx.status = 401;
  }
  if (ctx.isAuthenticated()) {
    try {
      // get latest version of boiler plate todos
      const newTodos = await todoQueries.findAll();
      // get current user by Id
      let currentUser = await userQueries.findUserById(ctx.request.body.userId);
      // overwrite bioler plates todos to the current user todos
      currentUser.todos = newTodos;
      const response = await currentUser.save();
      ctx.body = response;
    } catch (error) {
      ctx.body = {
        err: error
      };
      ctx.status = 500;
    }
  }
  next();
};

module.exports = refreshMyTodos;
