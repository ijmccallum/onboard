const userTodoQueries = require("./userTodoQueries.js");

const removeTodo = async (ctx, next) => {
  if (!ctx.isAuthenticated()) {
    ctx.status = 401;
  }
  if (ctx.isAuthenticated()) {
    const response = await userTodoQueries.removeUserTodo(
      ctx.request.body.userId,
      ctx.request.body.todoId
    );
    ctx.body = response;
  }
  next();
};

module.exports = removeTodo;
