const router = require("koa-router")();
const putTodoDone = require("./put.js");
const getTodo = require("./get.js");
const postNewTodo = require("./post.js");
const deleteTodo = require("./delete.js");
const refreshTodos = require("./refresh.js");

/** Get todo for specific user, require user email for id and id of todo */
router.get(
  `${process.env.API_ROOT}/users/todos/todo/:userEmail/:todoId`,
  getTodo
);
/** Put todo setup for future use */
router.put(`${process.env.API_ROOT}/users/todos/todo`, putTodoDone);
/** Post new todo to the currentUser todos  */
router.post(`${process.env.API_ROOT}/users/todos/todo`, postNewTodo);
/** Delete todo from the currentUser todos */
router.delete(`${process.env.API_ROOT}/users/todos/todo`, deleteTodo);
/** Reset/Refresh todos for the current user */
router.post(`${process.env.API_ROOT}/users/todos/refresh`, refreshTodos);

module.exports = router;
