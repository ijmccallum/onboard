const UserModel = require("../userSchema.js");
const userQueries = require("../userQueries.js");

const getUserTodoById = (userEmail, todoId) => {
  return new Promise((resolve, reject) => {
    userQueries
      .findByWiproEmail(userEmail)
      .then(user => {
        console.log("USER: ", user);
        const userTodo = user.todos.filter(todo => todo._id == todoId);
        //todo is some kind of mongoose object
        //todo._id is an object
        //todo.id is a string
        resolve(userTodo[0]);
      })
      .catch(error => {
        reject(error);
      });
  });
};

const updateUserTodo = (userId, todoId, todoDone) => {
  return new Promise((resolve, reject) => {
    UserModel.update(
      { _id: userId, "todos._id": todoId },
      {
        $set: {
          "todos.$.done": todoDone
        }
      },
      false,
      (error, response) => {
        if (error) {
          reject(error);
        }
        resolve(response);
      }
    );
  });
};

const removeUserTodo = (userId, todoId) => {
  return new Promise((resolve, reject) => {
    UserModel.update(
      { _id: userId },
      {
        $pull: { todos: { _id: todoId } }
      },
      false,
      (error, response) => {
        if (error) {
          reject(error);
        }
        resolve(response);
      }
    );
  });
};

module.exports = { getUserTodoById, updateUserTodo, removeUserTodo };
