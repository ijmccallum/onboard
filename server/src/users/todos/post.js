const userQueries = require("../../users/userQueries.js");

const postNewTodo = async (ctx, next) => {
  if (!ctx.isAuthenticated()) {
    ctx.status = 401;
  }
  if (ctx.isAuthenticated()) {
    if (!ctx.request.body.data.userId) {
      ctx.body = {
        error: "a user id is required to add a new todo"
      };
      ctx.status = 400;
    }
    let currentUser = await userQueries.findById(ctx.request.body.data.userId);
    currentUser.todos.push(ctx.request.body.data.newTodo);
    const response = await currentUser.save();
    const todosLength = response.todos.length - 1;
    const responseTodo = response.todos[todosLength];
    ctx.body = responseTodo;
  }
  next();
};

module.exports = postNewTodo;
