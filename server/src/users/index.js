const router = require("koa-router")();
const postUser = require("./post.js");
const getUsers = require("./get.js");
const deleteUsers = require("./delete.js");
require("./userSchema.js"); //runs on require?

router
  .post(`${process.env.API_ROOT}/users`, postUser)
  .get(`${process.env.API_ROOT}/users`, getUsers)
  .delete(`${process.env.API_ROOT}/users`, deleteUsers);

module.exports = router;
