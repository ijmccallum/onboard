const hasher = require("../hasher");
const todoQueries = require("../todos/todoQueries.js");
const userQueries = require("./userQueries.js");

const postUser = async (ctx, next) => {
  //create a user, seed their todos array with the boilerplate todos
  if (!ctx.isAuthenticated()) {
    ctx.status = 401;
  }
  if (ctx.isAuthenticated()) {
    if (!ctx.request.body.wiproemail || !ctx.request.body.password) {
      ctx.body = {
        error: "a wipro email / password are required to create a new user"
      };
      ctx.status = 400;
    } else {
      const newUserData = {
        wiproemail: ctx.request.body.wiproemail,
        password: await hasher.hash(ctx.request.body.password)
      };
      if (ctx.request.body.name) {
        newUserData.name = ctx.request.body.name;
      }
      if (ctx.request.body.personalemail) {
        newUserData.name = ctx.request.body.personalemail;
      }

      try {
        const seedTodos = await todoQueries.findAll();
        newUserData.todos = seedTodos;
        const newUser = await userQueries.create(newUserData);

        ctx.body = {
          user: newUser
        };
      } catch (err) {
        ctx.body = {
          err: err
        };
        ctx.status = 500;
      }
    }
  }
  next();
};
module.exports = postUser;
