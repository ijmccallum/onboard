const router = require("koa-router")();
const postLogout = require("./post.js");

router.post(`${process.env.API_ROOT}/logout`, postLogout);

module.exports = router;
